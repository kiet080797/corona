module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/theme */ "./styles/theme.ts");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-i18next */ "next-i18next");
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_i18next__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\pages\\_app.tsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const MyApp = ({
  Component,
  pageProps
}) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["ThemeProvider"], {
  theme: _styles_theme__WEBPACK_IMPORTED_MODULE_2__["theme"],
  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Component, _objectSpread({}, pageProps), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 5
  }, undefined)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 8,
  columnNumber: 3
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Object(next_i18next__WEBPACK_IMPORTED_MODULE_4__["appWithTranslation"])(MyApp));

/***/ }),

/***/ "./styles/theme.ts":
/*!*************************!*\
  !*** ./styles/theme.ts ***!
  \*************************/
/*! exports provided: theme */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "theme", function() { return theme; });
const theme = {
  fontSizes: [12, 14, 16, 18, 20, 22, 24],
  colors: {
    primary: "blue",
    secondary: "grey",
    white: "#fff",
    bigStone: "#1C2D41",
    outerSpace: "#202E2E",
    chelseaCucumber: "#7DA751",
    chelseaCucumber18: "rgba(125, 167, 81, 0.18)",
    mistGray: "#C2C2B5"
  },
  images: {
    avatar: {
      width: 100,
      height: 100,
      borderRadius: 10
    },
    symptoms: {
      m: 10,
      mb: 0,
      height: 110,
      borderRadius: 10
    },
    boarding: {
      width: "100%",
      height: 270
    },
    corona: {
      width: 150,
      height: 25
    },
    person: {
      width: 111,
      height: 130
    },
    diagnosis: {
      mt: 25,
      mb: 15,
      width: 150,
      height: 140
    }
  },
  buttons: {
    primary: {
      bg: "#fff",
      borderRadius: 23,
      height: 45,
      width: 90,
      cursor: "pointer",
      transition: "all 0.3s ease-in-out 0s",
      opacity: 0.8,
      boxShadow: "rgb(0 0 0 / 10%) 1px 4px 4px 1px",
      ":hover": {
        opacity: 1
      },
      ":active": {
        transform: "scale(0.95)"
      }
    },
    secondary: {
      bg: "chelseaCucumber",
      borderRadius: 150,
      height: 40,
      width: 135,
      cursor: "pointer",
      transition: "all 0.3s ease-in-out 0s",
      opacity: 0.8,
      boxShadow: "rgb(0 0 0 / 10%) 1px 6px 6px 1px",
      ":hover": {
        opacity: 1
      },
      ":active": {
        transform: "scale(0.95)"
      }
    },
    tertiary: {
      bg: "chelseaCucumber",
      borderRadius: 150,
      height: 45,
      width: 120,
      cursor: "pointer",
      transition: "all 0.3s ease-in-out 0s",
      opacity: 0.9,
      boxShadow: "rgb(0 0 0 / 10%) 1px 6px 6px 1px",
      ":hover": {
        opacity: 1
      },
      ":active": {
        transform: "scale(0.95)"
      }
    },
    phone: {
      mt: "5px",
      bg: "chelseaCucumber",
      borderRadius: 150,
      height: 45,
      width: 200,
      cursor: "pointer",
      transition: "all 0.3s ease-in-out 0s",
      opacity: 0.9,
      boxShadow: "rgb(0 0 0 / 10%) 1px 6px 6px 1px",
      ":hover": {
        opacity: 1
      },
      ":active": {
        transform: "scale(0.95)"
      }
    }
  },
  fontWeights: {
    body: 500,
    heading: 900
  }
};

/***/ }),

/***/ 0:
/*!*****************************************!*\
  !*** multi private-next-pages/_app.tsx ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.tsx */"./pages/_app.tsx");


/***/ }),

/***/ "next-i18next":
/*!*******************************!*\
  !*** external "next-i18next" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-i18next");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "theme-ui":
/*!***************************!*\
  !*** external "theme-ui" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("theme-ui");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvX2FwcC50c3giLCJ3ZWJwYWNrOi8vLy4vc3R5bGVzL3RoZW1lLnRzIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQtaTE4bmV4dFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwidGhlbWUtdWlcIiJdLCJuYW1lcyI6WyJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsInRoZW1lIiwiYXBwV2l0aFRyYW5zbGF0aW9uIiwiZm9udFNpemVzIiwiY29sb3JzIiwicHJpbWFyeSIsInNlY29uZGFyeSIsIndoaXRlIiwiYmlnU3RvbmUiLCJvdXRlclNwYWNlIiwiY2hlbHNlYUN1Y3VtYmVyIiwiY2hlbHNlYUN1Y3VtYmVyMTgiLCJtaXN0R3JheSIsImltYWdlcyIsImF2YXRhciIsIndpZHRoIiwiaGVpZ2h0IiwiYm9yZGVyUmFkaXVzIiwic3ltcHRvbXMiLCJtIiwibWIiLCJib2FyZGluZyIsImNvcm9uYSIsInBlcnNvbiIsImRpYWdub3NpcyIsIm10IiwiYnV0dG9ucyIsImJnIiwiY3Vyc29yIiwidHJhbnNpdGlvbiIsIm9wYWNpdHkiLCJib3hTaGFkb3ciLCJ0cmFuc2Zvcm0iLCJ0ZXJ0aWFyeSIsInBob25lIiwiZm9udFdlaWdodHMiLCJib2R5IiwiaGVhZGluZyJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTUEsS0FBbUIsR0FBRyxDQUFDO0FBQUVDLFdBQUY7QUFBYUM7QUFBYixDQUFELGtCQUMxQixxRUFBQyxzREFBRDtBQUFlLE9BQUssRUFBRUMsbURBQXRCO0FBQUEseUJBQ0UscUVBQUMsU0FBRCxvQkFBZUQsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURGOztBQU1lRSxzSUFBa0IsQ0FBQ0osS0FBRCxDQUFqQyxFOzs7Ozs7Ozs7Ozs7QUNYQTtBQUFBO0FBQU8sTUFBTUcsS0FBWSxHQUFHO0FBQzFCRSxXQUFTLEVBQUUsQ0FBQyxFQUFELEVBQUssRUFBTCxFQUFTLEVBQVQsRUFBYSxFQUFiLEVBQWlCLEVBQWpCLEVBQXFCLEVBQXJCLEVBQXlCLEVBQXpCLENBRGU7QUFHMUJDLFFBQU0sRUFBRTtBQUNOQyxXQUFPLEVBQUUsTUFESDtBQUVOQyxhQUFTLEVBQUUsTUFGTDtBQUdOQyxTQUFLLEVBQUUsTUFIRDtBQUlOQyxZQUFRLEVBQUUsU0FKSjtBQUtOQyxjQUFVLEVBQUUsU0FMTjtBQU1OQyxtQkFBZSxFQUFFLFNBTlg7QUFPTkMscUJBQWlCLEVBQUUsMEJBUGI7QUFRTkMsWUFBUSxFQUFFO0FBUkosR0FIa0I7QUFjMUJDLFFBQU0sRUFBRTtBQUNOQyxVQUFNLEVBQUU7QUFDTkMsV0FBSyxFQUFFLEdBREQ7QUFFTkMsWUFBTSxFQUFFLEdBRkY7QUFHTkMsa0JBQVksRUFBRTtBQUhSLEtBREY7QUFNTkMsWUFBUSxFQUFFO0FBQ1JDLE9BQUMsRUFBRSxFQURLO0FBRVJDLFFBQUUsRUFBRSxDQUZJO0FBR1JKLFlBQU0sRUFBRSxHQUhBO0FBSVJDLGtCQUFZLEVBQUU7QUFKTixLQU5KO0FBWU5JLFlBQVEsRUFBRTtBQUNSTixXQUFLLEVBQUUsTUFEQztBQUVSQyxZQUFNLEVBQUU7QUFGQSxLQVpKO0FBZ0JOTSxVQUFNLEVBQUU7QUFDTlAsV0FBSyxFQUFFLEdBREQ7QUFFTkMsWUFBTSxFQUFFO0FBRkYsS0FoQkY7QUFvQk5PLFVBQU0sRUFBRTtBQUNOUixXQUFLLEVBQUUsR0FERDtBQUVOQyxZQUFNLEVBQUU7QUFGRixLQXBCRjtBQXdCTlEsYUFBUyxFQUFFO0FBQ1RDLFFBQUUsRUFBRSxFQURLO0FBRVRMLFFBQUUsRUFBRSxFQUZLO0FBR1RMLFdBQUssRUFBRSxHQUhFO0FBSVRDLFlBQU0sRUFBRTtBQUpDO0FBeEJMLEdBZGtCO0FBOEMxQlUsU0FBTyxFQUFFO0FBQ1ByQixXQUFPLEVBQUU7QUFDUHNCLFFBQUUsRUFBRSxNQURHO0FBRVBWLGtCQUFZLEVBQUUsRUFGUDtBQUdQRCxZQUFNLEVBQUUsRUFIRDtBQUlQRCxXQUFLLEVBQUUsRUFKQTtBQUtQYSxZQUFNLEVBQUUsU0FMRDtBQU1QQyxnQkFBVSxFQUFFLHlCQU5MO0FBT1BDLGFBQU8sRUFBRSxHQVBGO0FBUVBDLGVBQVMsRUFBRSxrQ0FSSjtBQVNQLGdCQUFVO0FBQ1JELGVBQU8sRUFBRTtBQURELE9BVEg7QUFZUCxpQkFBVztBQUNURSxpQkFBUyxFQUFFO0FBREY7QUFaSixLQURGO0FBaUJQMUIsYUFBUyxFQUFFO0FBQ1RxQixRQUFFLEVBQUUsaUJBREs7QUFFVFYsa0JBQVksRUFBRSxHQUZMO0FBR1RELFlBQU0sRUFBRSxFQUhDO0FBSVRELFdBQUssRUFBRSxHQUpFO0FBS1RhLFlBQU0sRUFBRSxTQUxDO0FBTVRDLGdCQUFVLEVBQUUseUJBTkg7QUFPVEMsYUFBTyxFQUFFLEdBUEE7QUFRVEMsZUFBUyxFQUFFLGtDQVJGO0FBU1QsZ0JBQVU7QUFDUkQsZUFBTyxFQUFFO0FBREQsT0FURDtBQVlULGlCQUFXO0FBQ1RFLGlCQUFTLEVBQUU7QUFERjtBQVpGLEtBakJKO0FBaUNQQyxZQUFRLEVBQUU7QUFDUk4sUUFBRSxFQUFFLGlCQURJO0FBRVJWLGtCQUFZLEVBQUUsR0FGTjtBQUdSRCxZQUFNLEVBQUUsRUFIQTtBQUlSRCxXQUFLLEVBQUUsR0FKQztBQUtSYSxZQUFNLEVBQUUsU0FMQTtBQU1SQyxnQkFBVSxFQUFFLHlCQU5KO0FBT1JDLGFBQU8sRUFBRSxHQVBEO0FBUVJDLGVBQVMsRUFBRSxrQ0FSSDtBQVNSLGdCQUFVO0FBQ1JELGVBQU8sRUFBRTtBQURELE9BVEY7QUFZUixpQkFBVztBQUNURSxpQkFBUyxFQUFFO0FBREY7QUFaSCxLQWpDSDtBQWlEUEUsU0FBSyxFQUFFO0FBQ0xULFFBQUUsRUFBRSxLQURDO0FBRUxFLFFBQUUsRUFBRSxpQkFGQztBQUdMVixrQkFBWSxFQUFFLEdBSFQ7QUFJTEQsWUFBTSxFQUFFLEVBSkg7QUFLTEQsV0FBSyxFQUFFLEdBTEY7QUFNTGEsWUFBTSxFQUFFLFNBTkg7QUFPTEMsZ0JBQVUsRUFBRSx5QkFQUDtBQVFMQyxhQUFPLEVBQUUsR0FSSjtBQVNMQyxlQUFTLEVBQUUsa0NBVE47QUFVTCxnQkFBVTtBQUNSRCxlQUFPLEVBQUU7QUFERCxPQVZMO0FBYUwsaUJBQVc7QUFDVEUsaUJBQVMsRUFBRTtBQURGO0FBYk47QUFqREEsR0E5Q2lCO0FBaUgxQkcsYUFBVyxFQUFFO0FBQ1hDLFFBQUksRUFBRSxHQURLO0FBRVhDLFdBQU8sRUFBRTtBQUZFO0FBakhhLENBQXJCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRFAseUM7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsa0Q7Ozs7Ozs7Ozs7O0FDQUEscUMiLCJmaWxlIjoicGFnZXMvX2FwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAwKTtcbiIsImltcG9ydCBSZWFjdCwgeyBGQyB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgdHlwZSB7IEFwcFByb3BzIH0gZnJvbSBcIm5leHQvYXBwXCI7XHJcbmltcG9ydCB7IHRoZW1lIH0gZnJvbSBcIi4uL3N0eWxlcy90aGVtZVwiO1xyXG5pbXBvcnQgeyBUaGVtZVByb3ZpZGVyIH0gZnJvbSBcInRoZW1lLXVpXCI7XHJcbmltcG9ydCB7IGFwcFdpdGhUcmFuc2xhdGlvbiB9IGZyb20gXCJuZXh0LWkxOG5leHRcIjtcclxuXHJcbmNvbnN0IE15QXBwOiBGQzxBcHBQcm9wcz4gPSAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9KSA9PiAoXHJcbiAgPFRoZW1lUHJvdmlkZXIgdGhlbWU9e3RoZW1lfT5cclxuICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz5cclxuICA8L1RoZW1lUHJvdmlkZXI+XHJcbik7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBhcHBXaXRoVHJhbnNsYXRpb24oTXlBcHApO1xyXG4iLCJpbXBvcnQgeyBUaGVtZSB9IGZyb20gXCJ0aGVtZS11aVwiO1xyXG5leHBvcnQgY29uc3QgdGhlbWU6IFRoZW1lID0ge1xyXG4gIGZvbnRTaXplczogWzEyLCAxNCwgMTYsIDE4LCAyMCwgMjIsIDI0XSxcclxuXHJcbiAgY29sb3JzOiB7XHJcbiAgICBwcmltYXJ5OiBcImJsdWVcIixcclxuICAgIHNlY29uZGFyeTogXCJncmV5XCIsXHJcbiAgICB3aGl0ZTogXCIjZmZmXCIsXHJcbiAgICBiaWdTdG9uZTogXCIjMUMyRDQxXCIsXHJcbiAgICBvdXRlclNwYWNlOiBcIiMyMDJFMkVcIixcclxuICAgIGNoZWxzZWFDdWN1bWJlcjogXCIjN0RBNzUxXCIsXHJcbiAgICBjaGVsc2VhQ3VjdW1iZXIxODogXCJyZ2JhKDEyNSwgMTY3LCA4MSwgMC4xOClcIixcclxuICAgIG1pc3RHcmF5OiBcIiNDMkMyQjVcIixcclxuICB9LFxyXG5cclxuICBpbWFnZXM6IHtcclxuICAgIGF2YXRhcjoge1xyXG4gICAgICB3aWR0aDogMTAwLFxyXG4gICAgICBoZWlnaHQ6IDEwMCxcclxuICAgICAgYm9yZGVyUmFkaXVzOiAxMCxcclxuICAgIH0sXHJcbiAgICBzeW1wdG9tczoge1xyXG4gICAgICBtOiAxMCxcclxuICAgICAgbWI6IDAsXHJcbiAgICAgIGhlaWdodDogMTEwLFxyXG4gICAgICBib3JkZXJSYWRpdXM6IDEwLFxyXG4gICAgfSxcclxuICAgIGJvYXJkaW5nOiB7XHJcbiAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgaGVpZ2h0OiAyNzAsXHJcbiAgICB9LFxyXG4gICAgY29yb25hOiB7XHJcbiAgICAgIHdpZHRoOiAxNTAsXHJcbiAgICAgIGhlaWdodDogMjUsXHJcbiAgICB9LFxyXG4gICAgcGVyc29uOiB7XHJcbiAgICAgIHdpZHRoOiAxMTEsXHJcbiAgICAgIGhlaWdodDogMTMwLFxyXG4gICAgfSxcclxuICAgIGRpYWdub3Npczoge1xyXG4gICAgICBtdDogMjUsXHJcbiAgICAgIG1iOiAxNSxcclxuICAgICAgd2lkdGg6IDE1MCxcclxuICAgICAgaGVpZ2h0OiAxNDAsXHJcbiAgICB9LFxyXG4gIH0sXHJcblxyXG4gIGJ1dHRvbnM6IHtcclxuICAgIHByaW1hcnk6IHtcclxuICAgICAgYmc6IFwiI2ZmZlwiLFxyXG4gICAgICBib3JkZXJSYWRpdXM6IDIzLFxyXG4gICAgICBoZWlnaHQ6IDQ1LFxyXG4gICAgICB3aWR0aDogOTAsXHJcbiAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgIHRyYW5zaXRpb246IFwiYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHNcIixcclxuICAgICAgb3BhY2l0eTogMC44LFxyXG4gICAgICBib3hTaGFkb3c6IFwicmdiKDAgMCAwIC8gMTAlKSAxcHggNHB4IDRweCAxcHhcIixcclxuICAgICAgXCI6aG92ZXJcIjoge1xyXG4gICAgICAgIG9wYWNpdHk6IDEsXHJcbiAgICAgIH0sXHJcbiAgICAgIFwiOmFjdGl2ZVwiOiB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBcInNjYWxlKDAuOTUpXCIsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgc2Vjb25kYXJ5OiB7XHJcbiAgICAgIGJnOiBcImNoZWxzZWFDdWN1bWJlclwiLFxyXG4gICAgICBib3JkZXJSYWRpdXM6IDE1MCxcclxuICAgICAgaGVpZ2h0OiA0MCxcclxuICAgICAgd2lkdGg6IDEzNSxcclxuICAgICAgY3Vyc29yOiBcInBvaW50ZXJcIixcclxuICAgICAgdHJhbnNpdGlvbjogXCJhbGwgMC4zcyBlYXNlLWluLW91dCAwc1wiLFxyXG4gICAgICBvcGFjaXR5OiAwLjgsXHJcbiAgICAgIGJveFNoYWRvdzogXCJyZ2IoMCAwIDAgLyAxMCUpIDFweCA2cHggNnB4IDFweFwiLFxyXG4gICAgICBcIjpob3ZlclwiOiB7XHJcbiAgICAgICAgb3BhY2l0eTogMSxcclxuICAgICAgfSxcclxuICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45NSlcIixcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgICB0ZXJ0aWFyeToge1xyXG4gICAgICBiZzogXCJjaGVsc2VhQ3VjdW1iZXJcIixcclxuICAgICAgYm9yZGVyUmFkaXVzOiAxNTAsXHJcbiAgICAgIGhlaWdodDogNDUsXHJcbiAgICAgIHdpZHRoOiAxMjAsXHJcbiAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgIHRyYW5zaXRpb246IFwiYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHNcIixcclxuICAgICAgb3BhY2l0eTogMC45LFxyXG4gICAgICBib3hTaGFkb3c6IFwicmdiKDAgMCAwIC8gMTAlKSAxcHggNnB4IDZweCAxcHhcIixcclxuICAgICAgXCI6aG92ZXJcIjoge1xyXG4gICAgICAgIG9wYWNpdHk6IDEsXHJcbiAgICAgIH0sXHJcbiAgICAgIFwiOmFjdGl2ZVwiOiB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBcInNjYWxlKDAuOTUpXCIsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgcGhvbmU6IHtcclxuICAgICAgbXQ6IFwiNXB4XCIsXHJcbiAgICAgIGJnOiBcImNoZWxzZWFDdWN1bWJlclwiLFxyXG4gICAgICBib3JkZXJSYWRpdXM6IDE1MCxcclxuICAgICAgaGVpZ2h0OiA0NSxcclxuICAgICAgd2lkdGg6IDIwMCxcclxuICAgICAgY3Vyc29yOiBcInBvaW50ZXJcIixcclxuICAgICAgdHJhbnNpdGlvbjogXCJhbGwgMC4zcyBlYXNlLWluLW91dCAwc1wiLFxyXG4gICAgICBvcGFjaXR5OiAwLjksXHJcbiAgICAgIGJveFNoYWRvdzogXCJyZ2IoMCAwIDAgLyAxMCUpIDFweCA2cHggNnB4IDFweFwiLFxyXG4gICAgICBcIjpob3ZlclwiOiB7XHJcbiAgICAgICAgb3BhY2l0eTogMSxcclxuICAgICAgfSxcclxuICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45NSlcIixcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgfSxcclxuICBmb250V2VpZ2h0czoge1xyXG4gICAgYm9keTogNTAwLFxyXG4gICAgaGVhZGluZzogOTAwLFxyXG4gIH0sXHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQtaTE4bmV4dFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidGhlbWUtdWlcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==