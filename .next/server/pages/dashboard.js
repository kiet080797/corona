module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/dashboard.tsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/CardButton.tsx":
/*!***********************************!*\
  !*** ./components/CardButton.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_whiteArrow_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../public/whiteArrow.svg */ "./public/whiteArrow.svg");

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\CardButton.tsx";




const CardButton = ({
  title,
  content,
  onClick
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    bg: "white",
    mx: 15,
    sx: {
      position: "relative",
      borderRadius: 16,
      flexDirection: "column",
      justifyContent: "space-between",
      boxShadow: "rgb(0 0 0 / 5%) 1px 8px 8px 1px"
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
      my: 18,
      ml: 20,
      mr: 80,
      color: "#4F4F7D",
      sx: {
        fontSize: 20,
        lineHeight: "30px",
        fontWeight: "heading"
      },
      children: title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
      color: "#4F4F7D",
      mx: 20,
      sx: {
        fontSize: 1,
        fontWeight: "600",
        opacity: 0.6
      },
      children: content
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mx: 20,
      sx: {
        justifyContent: "space-between",
        alignItems: "center"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        href: "https://tuoitre.vn/tim-kiem.htm?keywords=covid",
        target: "_blank",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Button"], {
          onClick: onClick,
          variant: "secondary",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
            mr: 20,
            sx: {
              fontSize: 14,
              color: "white",
              fontWeight: "600"
            },
            children: "See More"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 62,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_whiteArrow_svg__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 72,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 61,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
        src: "/images/person.PNG",
        variant: "person"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (CardButton);

/***/ }),

/***/ "./components/CardDiagnosis.tsx":
/*!**************************************!*\
  !*** ./components/CardDiagnosis.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\CardDiagnosis.tsx";



const CardDiagnosis = ({
  label,
  content,
  image
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    bg: "white",
    mx: 15,
    mb: 10,
    sx: {
      height: 300,
      width: 360,
      borderRadius: 16,
      flexDirection: "column",
      alignItems: "center",
      boxShadow: "rgb(0 0 0 / 5%) 1px 10px 10px 1px"
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
      src: image,
      variant: "diagnosis"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
      mx: 15,
      color: "bigStone",
      mb: "5px",
      sx: {
        fontSize: 15,
        fontWeight: "heading"
      },
      children: label
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
      mt: 10,
      mx: 15,
      color: "outerSpace",
      sx: {
        fontSize: 0,
        opacity: 0.6,
        textAlign: "center",
        width: 250
      },
      children: content
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 12,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (CardDiagnosis);

/***/ }),

/***/ "./components/CardImageSymptoms.tsx":
/*!******************************************!*\
  !*** ./components/CardImageSymptoms.tsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\CardImageSymptoms.tsx";



const CardImageSymptoms = ({
  image,
  name,
  description,
  onClick
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    onClick: onClick,
    bg: "white",
    sx: {
      height: 215,
      borderRadius: 16,
      flexDirection: "column",
      transition: "all 0.3s ease-in-out 0s",
      boxShadow: "rgb(0 0 0 / 5%) 1px 10px 10px 1px",
      cursor: "pointer",
      ":active": {
        transform: "scale(0.95)"
      }
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
      src: image,
      variant: "symptoms"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mx: 15,
      sx: {
        flexDirection: "column"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "bigStone",
        mb: "3px",
        sx: {
          fontSize: 1,
          fontWeight: "heading"
        },
        children: name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "outerSpace",
        sx: {
          fontSize: 0,
          opacity: 0.6
        },
        children: description
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 17,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (CardImageSymptoms);

/***/ }),

/***/ "./components/CardPrevention.tsx":
/*!***************************************!*\
  !*** ./components/CardPrevention.tsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_arrow_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../public/arrow.svg */ "./public/arrow.svg");

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\CardPrevention.tsx";




const CardPrevention = ({
  image,
  nameCard,
  description,
  numberDiscussions,
  onClick,
  onDiscussions
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    columns: [2, "1fr 2fr"],
    mx: 15,
    mb: 10,
    p: "7px",
    bg: "white",
    sx: {
      borderRadius: 16,
      cursor: "pointer",
      transition: "all 0.3s ease-in-out 0s",
      boxShadow: "rgb(0 0 0 / 5%) 1px 8px 8px 1px",
      ":active": {
        transform: "scale(0.95)"
      }
    },
    onClick: onClick,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
      src: image,
      variant: "avatar"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      my: "7px",
      sx: {
        flexDirection: "column"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "bigStone",
        mb: "5px",
        sx: {
          fontSize: 15,
          fontWeight: 700
        },
        children: nameCard
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "outerSpace",
        mb: 9,
        mr: "5px",
        sx: {
          fontSize: 0,
          opacity: 0.6
        },
        children: description
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
        sx: {
          alignItems: "center"
        },
        onClick: onDiscussions,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
          color: "chelseaCucumber",
          mr: 15,
          sx: {
            fontSize: 14
          },
          children: [numberDiscussions, " Discussions"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 55,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_arrow_svg__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 23,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (CardPrevention);

/***/ }),

/***/ "./components/CardVideoSymptoms.tsx":
/*!******************************************!*\
  !*** ./components/CardVideoSymptoms.tsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\CardVideoSymptoms.tsx";



const CardVideoSymptoms = ({
  video,
  name,
  description,
  link,
  onClick
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    href: link,
    target: "_blank",
    bg: "white",
    mx: 15,
    mb: 10,
    sx: {
      height: 270,
      borderRadius: 16,
      flexDirection: "column",
      transition: "all 0.3s ease-in-out 0s",
      boxShadow: "rgb(0 0 0 / 5%) 1px 10px 10px 1px",
      ":active": {
        transform: "scale(0.99)"
      }
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("iframe", {
      style: {
        borderRadius: 10,
        border: "white solid 0",
        margin: 10
      },
      src: video,
      height: 180,
      width: 310
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mx: 15,
      onClick: onClick,
      sx: {
        flexDirection: "column",
        cursor: "pointer"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "bigStone",
        mb: "5px",
        sx: {
          fontSize: 15,
          fontWeight: "heading"
        },
        children: name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "outerSpace",
        sx: {
          fontSize: 0,
          opacity: 0.6
        },
        children: description
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (CardVideoSymptoms);

/***/ }),

/***/ "./components/Footer.tsx":
/*!*******************************!*\
  !*** ./components/Footer.tsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _public_earth_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../public/earth.svg */ "./public/earth.svg");

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\Footer.tsx";




const Footer = ({
  onClick
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    bg: "white",
    px: 15,
    sx: {
      height: 70,
      justifyContent: "space-between",
      alignItems: "center",
      fontFamily: "Helvetica Now Text"
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Box"], {
      mr: 15,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_earth_svg__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mx: 10,
      sx: {
        width: 140,
        flexDirection: "column",
        justifyContent: "space-between"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "outerSpace",
        mb: "5px",
        sx: {
          fontSize: 2,
          fontWeight: "heading"
        },
        children: "Live Map info"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "outerSpace",
        sx: {
          fontSize: 0,
          opacity: 0.6,
          lineHeight: "16px"
        },
        children: "Watch the live update by each countries"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      href: "https://bando.tphcm.gov.vn/ogis",
      target: "_blank",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        onClick: onClick,
        variant: "tertiary",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
          sx: {
            fontSize: 1
          },
          children: "Watch Live"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 55,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./components/Selection.tsx":
/*!**********************************!*\
  !*** ./components/Selection.tsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\Selection.tsx";



const Selection = ({
  items,
  onClick,
  selectedItem
}) => {
  const {
    0: selected,
    1: setSelected
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(selectedItem !== null && selectedItem !== void 0 ? selectedItem : items[0]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (onClick) onClick(selected.value);
  }, [selected]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    bg: "white",
    mx: 15,
    mb: 10,
    p: "5px",
    sx: {
      height: 47,
      borderRadius: 25,
      boxShadow: "rgb(0 0 0 / 5%) 1px 8px 8px 1px"
    },
    children: items.map(item => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      onClick: () => {
        setSelected(item);
      },
      bg: selected === item ? "chelseaCucumber18" : "white",
      sx: {
        borderRadius: 25,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer"
      },
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        sx: {
          fontWeight: selected === item ? "700" : "body",
          fontSize: 15,
          lineHeight: 20
        },
        color: selected === item ? "chelseaCucumber" : "mistGray",
        children: item.label
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 48,
        columnNumber: 11
      }, undefined)
    }, item.id, false, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 9
    }, undefined))
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 22,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Selection);

/***/ }),

/***/ "./pages/dashboard.tsx":
/*!*****************************!*\
  !*** ./pages/dashboard.tsx ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_CardButton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/CardButton */ "./components/CardButton.tsx");
/* harmony import */ var _components_CardPrevention__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/CardPrevention */ "./components/CardPrevention.tsx");
/* harmony import */ var _components_CardVideoSymptoms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/CardVideoSymptoms */ "./components/CardVideoSymptoms.tsx");
/* harmony import */ var _components_CardImageSymptoms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/CardImageSymptoms */ "./components/CardImageSymptoms.tsx");
/* harmony import */ var _components_CardDiagnosis__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/CardDiagnosis */ "./components/CardDiagnosis.tsx");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/Footer */ "./components/Footer.tsx");
/* harmony import */ var _public_menu_svg__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../public/menu.svg */ "./public/menu.svg");
/* harmony import */ var _public_glass_svg__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../public/glass.svg */ "./public/glass.svg");
/* harmony import */ var _components_Selection__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/Selection */ "./components/Selection.tsx");
/* harmony import */ var _public_phone_svg__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../public/phone.svg */ "./public/phone.svg");

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\pages\\dashboard.tsx";













const Items = [{
  id: 1,
  label: "Prevention",
  value: "1"
}, {
  id: 2,
  label: "Symptoms",
  value: "2"
}, {
  id: 3,
  label: "Diagnosis",
  value: "3"
}];
const PreItems = [{
  image: "/images/logo1.PNG",
  nameCard: "Wear a facemask",
  description: "You should wear facemask when you are around other people",
  numberDiscussions: "153"
}, {
  image: "/images/logo2.PNG",
  nameCard: "Avoid close contact",
  description: "Put distance between yourself and other people",
  numberDiscussions: "127"
}, {
  image: "/images/logo3.PNG",
  nameCard: "Stay home if you’re sick",
  description: "Stay home if you are sick, except to get medical care.",
  numberDiscussions: "78"
}, {
  image: "/images/logo4.PNG",
  nameCard: "Clean your hands often",
  description: "Clean your hands often",
  numberDiscussions: "324"
}];

const dashboard = () => {
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"])();
  const {
    0: close,
    1: setClose
  } = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false);
  const {
    0: val,
    1: setVal
  } = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])("1");

  const handleClick = () => {
    if (!close) setClose(true);
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
    sx: {
      width: "100%",
      justifyContent: "center",
      alignItems: "center"
    },
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
      bg: "#F4F0EB",
      sx: {
        width: 360,
        height: 700,
        flexDirection: "column",
        overflowY: "auto"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
        bg: "#ED3D3D",
        sx: {
          height: close ? 90 : 200,
          width: "100%"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
          bg: "#ED3D3D",
          mt: 20,
          mx: 15,
          sx: {
            width: "100%",
            height: 30,
            justifyContent: "space-between",
            alignItems: "center",
            zIndex: 99
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
            sx: {
              cursor: "pointer",
              transition: "all 0.3s ease-in-out 0s",
              ":active": {
                transform: "scale(0.90)"
              }
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_menu_svg__WEBPACK_IMPORTED_MODULE_10__["default"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 114,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 105,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
            onClick: () => {
              router.reload();
            },
            sx: {
              cursor: "pointer",
              transition: "all 0.3s ease-in-out 0s",
              ":active": {
                transform: "scale(0.95)"
              }
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Image"], {
              src: "/images/corona.PNG",
              variant: "corona"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 128,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 116,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
            sx: {
              cursor: "pointer",
              transition: "all 0.3s ease-in-out 0s",
              ":active": {
                transform: "scale(0.95)"
              }
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_glass_svg__WEBPACK_IMPORTED_MODULE_11__["default"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 139,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 93,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, undefined), !close && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
        mt: -130,
        sx: {
          zIndex: 99
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardButton__WEBPACK_IMPORTED_MODULE_4__["default"], {
          title: "Coronavirus disease (COVID-19) advice for the public",
          content: "Stay aware of the latest information on the COVID-19 outbreak",
          onClick: handleClick
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 150,
          columnNumber: 13
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 144,
        columnNumber: 11
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
        mt: -160,
        sx: {
          height: 480,
          width: "100%"
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
          mt: close ? 137 : 185,
          sx: {
            width: "100%",
            flexDirection: "column"
          },
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
            sx: {
              width: "100%",
              flexDirection: "column"
            },
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Selection__WEBPACK_IMPORTED_MODULE_12__["default"], {
              items: Items,
              onClick: value => {
                setVal(value);
                if (!close && value !== "1") setClose(true);
              },
              selectedItem: Items[0]
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 177,
              columnNumber: 15
            }, undefined), val === "1" ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
              sx: {
                flexDirection: "column",
                width: "100%"
              },
              children: PreItems.map((item, index) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardPrevention__WEBPACK_IMPORTED_MODULE_5__["default"], {
                  onClick: handleClick,
                  image: item.image,
                  nameCard: item.nameCard,
                  description: item.description,
                  numberDiscussions: item.numberDiscussions
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 194,
                  columnNumber: 23
                }, undefined)
              }, index, false, {
                fileName: _jsxFileName,
                lineNumber: 193,
                columnNumber: 21
              }, undefined))
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 186,
              columnNumber: 17
            }, undefined) : val === "2" ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
              sx: {
                flexDirection: "column",
                width: "100%"
              },
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardVideoSymptoms__WEBPACK_IMPORTED_MODULE_6__["default"], {
                  link: "https://www.youtube.com/watch?v=WPF7Ka3tNSU",
                  video: "https://www.youtube.com/embed/WPF7Ka3tNSU",
                  name: "What Coronavirus Symptoms Look Like",
                  description: "After being exposed to the virus that cause COVID-19, it can take as 2 to 4 days to develop."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 212,
                  columnNumber: 21
                }, undefined)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 211,
                columnNumber: 19
              }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Grid"], {
                columns: 2,
                px: 15,
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardImageSymptoms__WEBPACK_IMPORTED_MODULE_7__["default"], {
                    image: "/images/ferver.PNG",
                    name: "Fever",
                    description: "He severity of COVID-19 symptoms can range from very mild to severe"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 221,
                    columnNumber: 23
                  }, undefined)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 220,
                  columnNumber: 21
                }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardImageSymptoms__WEBPACK_IMPORTED_MODULE_7__["default"], {
                    image: "/images/cough.PNG",
                    name: "Cough",
                    description: "Such as heart or lung disease or diabetes, may be at higher risk of serious illness"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 228,
                    columnNumber: 23
                  }, undefined)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 227,
                  columnNumber: 21
                }, undefined)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 219,
                columnNumber: 19
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 205,
              columnNumber: 17
            }, undefined) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
              sx: {
                flexDirection: "column",
                alignItems: "center"
              },
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                sx: {
                  width: 360
                },
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_CardDiagnosis__WEBPACK_IMPORTED_MODULE_8__["default"], {
                  image: "/images/Diagnosis.PNG",
                  label: "Book Test Apooitment",
                  content: "If you develop symptoms of coronavirus disease 2019 (C-OVID-19) and you've been exposed to the virus, contact your doctor."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 244,
                  columnNumber: 21
                }, undefined)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 243,
                columnNumber: 19
              }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Text"], {
                color: "bigStone",
                mt: 30,
                mb: "5px",
                sx: {
                  fontSize: 15,
                  fontWeight: "heading"
                },
                children: "Give a Miss Call On"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 250,
                columnNumber: 19
              }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "phone",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
                  sx: {
                    justifyContent: "center",
                    alignItems: "center"
                  },
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_public_phone_svg__WEBPACK_IMPORTED_MODULE_13__["default"], {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 268,
                    columnNumber: 23
                  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Text"], {
                    ml: 10,
                    children: "028-3930-9912"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 269,
                    columnNumber: 23
                  }, undefined)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 262,
                  columnNumber: 21
                }, undefined)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 261,
                columnNumber: 19
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 237,
              columnNumber: 17
            }, undefined)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 171,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_3__["Flex"], {
            sx: {
              position: close ? "fixed" : "relative",
              top: close ? 630 : 0,
              width: close ? 360 : 343
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Footer__WEBPACK_IMPORTED_MODULE_9__["default"], {
              onClick: handleClick
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 282,
              columnNumber: 15
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 275,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 157,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 7
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 70,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (dashboard);

/***/ }),

/***/ "./public/arrow.svg":
/*!**************************!*\
  !*** ./public/arrow.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _path;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgArrow(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 16.813,
    height: 10.869
  }, props), _path || (_path = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 4336",
    d: "M.877 6.311h12.936l-3.075 3.06a.877.877 0 001.237 1.243l4.58-4.558a.877.877 0 000-1.241L11.975.257a.877.877 0 00-1.237 1.242l3.075 3.059H.877a.876.876 0 100 1.753z",
    fill: "#7da751"
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgArrow);

/***/ }),

/***/ "./public/earth.svg":
/*!**************************!*\
  !*** ./public/earth.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _path, _path2, _path3, _path4, _path5, _path6, _path7;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgEarth(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 43.424,
    height: 47.104
  }, props), _path || (_path = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28566",
    d: "M37.144 35.513a.736.736 0 00-.995.307q-.065.123-.132.244h-2.949a24.421 24.421 0 00.829-2.775.736.736 0 10-1.433-.338 22.333 22.333 0 01-.978 3.113h-5.653a57.379 57.379 0 00.658-8.1h3.685a.736.736 0 100-1.472h-3.685a56.472 56.472 0 00-.78-8.832h5.475a22.04 22.04 0 01.893 2.421.736.736 0 101.408-.429q-.314-1.031-.7-1.992h2.806c.074.122.149.244.221.368a.736.736 0 101.274-.737l-.3-.493a.735.735 0 00-.306-.479 19.9 19.9 0 00-21.2-8.431.736.736 0 00.338 1.433q.716-.169 1.448-.279a18.9 18.9 0 00-2.76 7.147h-6.04a.732.732 0 00-.342 0H5.152q.274-.365.566-.718a.736.736 0 00-1.132-.941 19.725 19.725 0 00-1.323 1.789.735.735 0 00-.308.485 19.873 19.873 0 1034.5 19.7.736.736 0 00-.311-.991zm-6.308 2.023a17.369 17.369 0 01-3.18 4.573 11.887 11.887 0 01-4.8 3.051 20.945 20.945 0 002.716-7.625zm-5.408-21.344a19.754 19.754 0 00-2.567-6.887 13.779 13.779 0 017.608 6.887zm3.772-4.823a18.512 18.512 0 015.39 4.823h-2.466A18.243 18.243 0 0028.2 10.82q.507.259 1 .549zm-12.4 1.407c.912-2.47 2.062-3.944 3.075-3.944 1.187 0 2.48 1.9 3.46 5.085.219.713.418 1.474.6 2.275h-8.119a26.632 26.632 0 01.984-3.416zm-2.748 11.51a.735.735 0 00.779-.691 50.5 50.5 0 01.689-5.935h8.706a53.64 53.64 0 01.793 8.832h-3.675a.736.736 0 100 1.472h3.675a54.8 54.8 0 01-.666 8.1h-3.009a.736.736 0 100 1.472h2.741a31.36 31.36 0 01-.753 3.011c-.98 3.184-2.273 5.085-3.46 5.085-.993 0-2.084-1.358-2.993-3.725a.736.736 0 10-1.374.527 12.7 12.7 0 001.388 2.729c-3.307-1.059-6.144-3.843-7.993-7.63a.736.736 0 00-.068-1.469h-.579a23.921 23.921 0 01-1.617-8.1H8.1a.736.736 0 100-1.472H6.635a23.683 23.683 0 011.927-8.832h5.466a52.464 52.464 0 00-.67 5.844.736.736 0 00.691.782zM1.491 27.968h3.675a25.532 25.532 0 001.511 8.1H3.734a18.274 18.274 0 01-2.246-8.1zm5.476-10.3a25.37 25.37 0 00-1.8 8.832h-3.68a18.442 18.442 0 012.669-8.84zM4.638 37.54h2.639a20.329 20.329 0 002.115 3.661 17.029 17.029 0 002.159 2.441 18.531 18.531 0 01-6.916-6.106zm23.525 6.12q.273-.252.539-.52a18.973 18.973 0 003.768-5.608h2.648a18.371 18.371 0 01-6.955 6.128z",
    fill: "#c8af91"
  })), _path2 || (_path2 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28567",
    d: "M7.668 14.583a.736.736 0 00.863-.005c.223-.163 5.453-4.046 5.453-8.69a5.888 5.888 0 00-11.776 0c0 4.74 5.237 8.536 5.46 8.695zm.428-13.111a4.421 4.421 0 014.412 4.416c0 3.2-3.233 6.174-4.421 7.16-1.188-.969-4.411-3.9-4.411-7.16a4.421 4.421 0 014.42-4.416z",
    fill: "#ed3d3d"
  })), _path3 || (_path3 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28568",
    d: "M11.04 5.888a.736.736 0 00-1.472 0 1.472 1.472 0 11-1.472-1.472.736.736 0 100-1.472 2.944 2.944 0 102.944 2.944z",
    fill: "#ed3d3d"
  })), _path4 || (_path4 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28569",
    d: "M37.536 19.872a5.9 5.9 0 00-5.888 5.888c0 4.74 5.237 8.536 5.46 8.695a.736.736 0 00.863-.005c.223-.163 5.453-4.046 5.453-8.69a5.9 5.9 0 00-5.888-5.888zm0 13.048c-1.188-.969-4.411-3.9-4.411-7.16a4.416 4.416 0 118.832 0c-.009 3.196-3.238 6.174-4.426 7.16z",
    fill: "#7da751"
  })), _path5 || (_path5 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28570",
    d: "M39.744 25.024a.736.736 0 00-.736.736 1.472 1.472 0 11-1.472-1.472.736.736 0 100-1.472 2.944 2.944 0 102.944 2.944.736.736 0 00-.736-.736z",
    fill: "#7da751"
  })), _path6 || (_path6 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28571",
    d: "M8.832 31.648c0 4.74 5.237 8.536 5.46 8.695a.736.736 0 00.863-.005c.223-.163 5.453-4.046 5.453-8.69a5.888 5.888 0 10-11.776 0zm5.888-4.416a4.421 4.421 0 014.416 4.416c0 3.2-3.233 6.174-4.421 7.16-1.188-.969-4.411-3.9-4.411-7.16a4.421 4.421 0 014.416-4.416z",
    fill: "#c8af91"
  })), _path7 || (_path7 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 28572",
    d: "M14.72 34.592a2.947 2.947 0 002.944-2.944.736.736 0 00-1.472 0 1.472 1.472 0 11-1.472-1.472.736.736 0 100-1.472 2.944 2.944 0 100 5.888z",
    fill: "#c8af91"
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgEarth);

/***/ }),

/***/ "./public/glass.svg":
/*!**************************!*\
  !*** ./public/glass.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _g, _g2;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgGlass(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 22.8,
    height: 22.8
  }, props), _g || (_g = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 2528"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 2527"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 1543",
    d: "M10.088.4a9.688 9.688 0 109.688 9.688A9.7 9.7 0 0010.088.4zm0 17.588a7.9 7.9 0 117.9-7.9 7.909 7.909 0 01-7.9 7.9z",
    fill: "#fff",
    stroke: "#fff",
    strokeWidth: 0.8
  })))), _g2 || (_g2 = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 2530"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 2529"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 1544",
    d: "M22.138 20.873l-5.127-5.127a.894.894 0 10-1.265 1.265l5.127 5.127a.894.894 0 001.265-1.265z",
    fill: "#fff",
    stroke: "#fff",
    strokeWidth: 0.8
  })))));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgGlass);

/***/ }),

/***/ "./public/menu.svg":
/*!*************************!*\
  !*** ./public/menu.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _g;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgMenu(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 25,
    height: 12
  }, props), _g || (_g = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 2525",
    fill: "#fff",
    transform: "translate(-15 -27)"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("rect", {
    "data-name": "Rectangle 306",
    width: 25,
    height: 3,
    rx: 1.5,
    transform: "translate(15 27)"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 4347",
    d: "M16.5 36h13a1.5 1.5 0 010 3h-13a1.5 1.5 0 010-3z"
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgMenu);

/***/ }),

/***/ "./public/phone.svg":
/*!**************************!*\
  !*** ./public/phone.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _path;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgPhone(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 24,
    height: 24,
    viewBox: "0 0 27 27"
  }, props), _path || (_path = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    d: "M23.044 3.952A13.5 13.5 0 1027 13.5a13.5 13.5 0 00-3.956-9.548zm-2.6 14.853l-.684.679a3.6 3.6 0 01-3.4.972 11.962 11.962 0 01-3.472-1.556 16.051 16.051 0 01-2.741-2.21A16.174 16.174 0 018.1 14.2a12.6 12.6 0 01-1.539-3.114 3.6 3.6 0 01.9-3.677l.8-.8a.571.571 0 01.808 0L11.6 9.141a.571.571 0 010 .808l-1.485 1.485a1.21 1.21 0 00-.126 1.575 17.367 17.367 0 001.778 2.075 17.261 17.261 0 002.34 1.967 1.219 1.219 0 001.557-.135l1.436-1.457a.571.571 0 01.808 0L20.443 18a.571.571 0 01.002.805z",
    fill: "#fff"
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgPhone);

/***/ }),

/***/ "./public/whiteArrow.svg":
/*!*******************************!*\
  !*** ./public/whiteArrow.svg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _g;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



function SvgWhiteArrow(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 16.813,
    height: 10.869
  }, props), _g || (_g = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("g", {
    "data-name": "Group 3549"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("path", {
    "data-name": "Path 4336",
    d: "M.877 6.311h12.936l-3.075 3.06a.877.877 0 001.237 1.243l4.58-4.558a.877.877 0 000-1.241L11.975.257a.877.877 0 00-1.237 1.242l3.075 3.059H.877a.876.876 0 100 1.753z",
    fill: "#fff"
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (SvgWhiteArrow);

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "theme-ui":
/*!***************************!*\
  !*** external "theme-ui" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("theme-ui");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9DYXJkQnV0dG9uLnRzeCIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0NhcmREaWFnbm9zaXMudHN4Iiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvQ2FyZEltYWdlU3ltcHRvbXMudHN4Iiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvQ2FyZFByZXZlbnRpb24udHN4Iiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvQ2FyZFZpZGVvU3ltcHRvbXMudHN4Iiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvRm9vdGVyLnRzeCIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1NlbGVjdGlvbi50c3giLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvZGFzaGJvYXJkLnRzeCIsIndlYnBhY2s6Ly8vLi9wdWJsaWMvYXJyb3cuc3ZnIiwid2VicGFjazovLy8uL3B1YmxpYy9lYXJ0aC5zdmciLCJ3ZWJwYWNrOi8vLy4vcHVibGljL2dsYXNzLnN2ZyIsIndlYnBhY2s6Ly8vLi9wdWJsaWMvbWVudS5zdmciLCJ3ZWJwYWNrOi8vLy4vcHVibGljL3Bob25lLnN2ZyIsIndlYnBhY2s6Ly8vLi9wdWJsaWMvd2hpdGVBcnJvdy5zdmciLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInRoZW1lLXVpXCIiXSwibmFtZXMiOlsiQ2FyZEJ1dHRvbiIsInRpdGxlIiwiY29udGVudCIsIm9uQ2xpY2siLCJwb3NpdGlvbiIsImJvcmRlclJhZGl1cyIsImZsZXhEaXJlY3Rpb24iLCJqdXN0aWZ5Q29udGVudCIsImJveFNoYWRvdyIsImZvbnRTaXplIiwibGluZUhlaWdodCIsImZvbnRXZWlnaHQiLCJvcGFjaXR5IiwiYWxpZ25JdGVtcyIsImNvbG9yIiwiQ2FyZERpYWdub3NpcyIsImxhYmVsIiwiaW1hZ2UiLCJoZWlnaHQiLCJ3aWR0aCIsInRleHRBbGlnbiIsIkNhcmRJbWFnZVN5bXB0b21zIiwibmFtZSIsImRlc2NyaXB0aW9uIiwidHJhbnNpdGlvbiIsImN1cnNvciIsInRyYW5zZm9ybSIsIkNhcmRQcmV2ZW50aW9uIiwibmFtZUNhcmQiLCJudW1iZXJEaXNjdXNzaW9ucyIsIm9uRGlzY3Vzc2lvbnMiLCJDYXJkVmlkZW9TeW1wdG9tcyIsInZpZGVvIiwibGluayIsImJvcmRlciIsIm1hcmdpbiIsIkZvb3RlciIsImZvbnRGYW1pbHkiLCJTZWxlY3Rpb24iLCJpdGVtcyIsInNlbGVjdGVkSXRlbSIsInNlbGVjdGVkIiwic2V0U2VsZWN0ZWQiLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsInZhbHVlIiwibWFwIiwiaXRlbSIsImZsZXgiLCJpZCIsIkl0ZW1zIiwiUHJlSXRlbXMiLCJkYXNoYm9hcmQiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJjbG9zZSIsInNldENsb3NlIiwidmFsIiwic2V0VmFsIiwiaGFuZGxlQ2xpY2siLCJvdmVyZmxvd1kiLCJ6SW5kZXgiLCJyZWxvYWQiLCJpbmRleCIsInRvcCJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFDQTtBQUNBOztBQVFBLE1BQU1BLFVBQStCLEdBQUcsQ0FBQztBQUFFQyxPQUFGO0FBQVNDLFNBQVQ7QUFBa0JDO0FBQWxCLENBQUQsS0FBaUM7QUFDdkUsc0JBQ0UscUVBQUMsNkNBQUQ7QUFDRSxNQUFFLEVBQUMsT0FETDtBQUVFLE1BQUUsRUFBRSxFQUZOO0FBR0UsTUFBRSxFQUFFO0FBQ0ZDLGNBQVEsRUFBRSxVQURSO0FBRUZDLGtCQUFZLEVBQUUsRUFGWjtBQUdGQyxtQkFBYSxFQUFFLFFBSGI7QUFJRkMsb0JBQWMsRUFBRSxlQUpkO0FBS0ZDLGVBQVMsRUFBRTtBQUxULEtBSE47QUFBQSw0QkFXRSxxRUFBQyw2Q0FBRDtBQUNFLFFBQUUsRUFBRSxFQUROO0FBRUUsUUFBRSxFQUFFLEVBRk47QUFHRSxRQUFFLEVBQUUsRUFITjtBQUlFLFdBQUssRUFBQyxTQUpSO0FBS0UsUUFBRSxFQUFFO0FBQ0ZDLGdCQUFRLEVBQUUsRUFEUjtBQUVGQyxrQkFBVSxFQUFFLE1BRlY7QUFHRkMsa0JBQVUsRUFBRTtBQUhWLE9BTE47QUFBQSxnQkFXR1Y7QUFYSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVhGLGVBd0JFLHFFQUFDLDZDQUFEO0FBQ0UsV0FBSyxFQUFDLFNBRFI7QUFFRSxRQUFFLEVBQUUsRUFGTjtBQUdFLFFBQUUsRUFBRTtBQUNGUSxnQkFBUSxFQUFFLENBRFI7QUFHRkUsa0JBQVUsRUFBRSxLQUhWO0FBSUZDLGVBQU8sRUFBRTtBQUpQLE9BSE47QUFBQSxnQkFVR1Y7QUFWSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXhCRixlQXFDRSxxRUFBQyw2Q0FBRDtBQUNFLFFBQUUsRUFBRSxFQUROO0FBRUUsUUFBRSxFQUFFO0FBQ0ZLLHNCQUFjLEVBQUUsZUFEZDtBQUVGTSxrQkFBVSxFQUFFO0FBRlYsT0FGTjtBQUFBLDhCQU9FLHFFQUFDLDZDQUFEO0FBQ0UsWUFBSSxFQUFDLGdEQURQO0FBRUUsY0FBTSxFQUFDLFFBRlQ7QUFBQSwrQkFJRSxxRUFBQywrQ0FBRDtBQUFRLGlCQUFPLEVBQUVWLE9BQWpCO0FBQTBCLGlCQUFPLEVBQUMsV0FBbEM7QUFBQSxrQ0FDRSxxRUFBQyw2Q0FBRDtBQUNFLGNBQUUsRUFBRSxFQUROO0FBRUUsY0FBRSxFQUFFO0FBQ0ZNLHNCQUFRLEVBQUUsRUFEUjtBQUVGSyxtQkFBSyxFQUFFLE9BRkw7QUFHRkgsd0JBQVUsRUFBRTtBQUhWLGFBRk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREYsZUFXRSxxRUFBQyw4REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQVhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBUEYsZUF5QkUscUVBQUMsOENBQUQ7QUFBTyxXQUFHLEVBQUMsb0JBQVg7QUFBZ0MsZUFBTyxFQUFDO0FBQXhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBekJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFyQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFtRUQsQ0FwRUQ7O0FBc0VlWCx5RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoRkE7QUFDQTs7QUFRQSxNQUFNZSxhQUFxQyxHQUFHLENBQUM7QUFBRUMsT0FBRjtBQUFTZCxTQUFUO0FBQWtCZTtBQUFsQixDQUFELEtBQStCO0FBQzNFLHNCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsTUFBRSxFQUFDLE9BREw7QUFFRSxNQUFFLEVBQUUsRUFGTjtBQUdFLE1BQUUsRUFBRSxFQUhOO0FBSUUsTUFBRSxFQUFFO0FBQ0ZDLFlBQU0sRUFBRSxHQUROO0FBRUZDLFdBQUssRUFBRSxHQUZMO0FBR0ZkLGtCQUFZLEVBQUUsRUFIWjtBQUlGQyxtQkFBYSxFQUFFLFFBSmI7QUFLRk8sZ0JBQVUsRUFBRSxRQUxWO0FBTUZMLGVBQVMsRUFBRTtBQU5ULEtBSk47QUFBQSw0QkFhRSxxRUFBQyw4Q0FBRDtBQUFPLFNBQUcsRUFBRVMsS0FBWjtBQUFtQixhQUFPLEVBQUM7QUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFiRixlQWNFLHFFQUFDLDZDQUFEO0FBQ0UsUUFBRSxFQUFFLEVBRE47QUFFRSxXQUFLLEVBQUMsVUFGUjtBQUdFLFFBQUUsRUFBQyxLQUhMO0FBSUUsUUFBRSxFQUFFO0FBQUVSLGdCQUFRLEVBQUUsRUFBWjtBQUFnQkUsa0JBQVUsRUFBRTtBQUE1QixPQUpOO0FBQUEsZ0JBTUdLO0FBTkg7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFkRixlQXNCRSxxRUFBQyw2Q0FBRDtBQUNFLFFBQUUsRUFBRSxFQUROO0FBRUUsUUFBRSxFQUFFLEVBRk47QUFHRSxXQUFLLEVBQUMsWUFIUjtBQUlFLFFBQUUsRUFBRTtBQUFFUCxnQkFBUSxFQUFFLENBQVo7QUFBZUcsZUFBTyxFQUFFLEdBQXhCO0FBQTZCUSxpQkFBUyxFQUFFLFFBQXhDO0FBQWtERCxhQUFLLEVBQUU7QUFBekQsT0FKTjtBQUFBLGdCQU1HakI7QUFOSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXRCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQWlDRCxDQWxDRDs7QUFvQ2VhLDRFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdDQTtBQUNBOztBQVFBLE1BQU1NLGlCQUE2QyxHQUFHLENBQUM7QUFDckRKLE9BRHFEO0FBRXJESyxNQUZxRDtBQUdyREMsYUFIcUQ7QUFJckRwQjtBQUpxRCxDQUFELEtBS2hEO0FBQ0osc0JBQ0UscUVBQUMsNkNBQUQ7QUFDRSxXQUFPLEVBQUVBLE9BRFg7QUFFRSxNQUFFLEVBQUMsT0FGTDtBQUdFLE1BQUUsRUFBRTtBQUNGZSxZQUFNLEVBQUUsR0FETjtBQUVGYixrQkFBWSxFQUFFLEVBRlo7QUFHRkMsbUJBQWEsRUFBRSxRQUhiO0FBSUZrQixnQkFBVSxFQUFFLHlCQUpWO0FBS0ZoQixlQUFTLEVBQUUsbUNBTFQ7QUFNRmlCLFlBQU0sRUFBRSxTQU5OO0FBT0YsaUJBQVc7QUFDVEMsaUJBQVMsRUFBRTtBQURGO0FBUFQsS0FITjtBQUFBLDRCQWVFLHFFQUFDLDhDQUFEO0FBQU8sU0FBRyxFQUFFVCxLQUFaO0FBQW1CLGFBQU8sRUFBQztBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWZGLGVBaUJFLHFFQUFDLDZDQUFEO0FBQU0sUUFBRSxFQUFFLEVBQVY7QUFBYyxRQUFFLEVBQUU7QUFBRVgscUJBQWEsRUFBRTtBQUFqQixPQUFsQjtBQUFBLDhCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLFVBRFI7QUFFRSxVQUFFLEVBQUMsS0FGTDtBQUdFLFVBQUUsRUFBRTtBQUFFRyxrQkFBUSxFQUFFLENBQVo7QUFBZUUsb0JBQVUsRUFBRTtBQUEzQixTQUhOO0FBQUEsa0JBS0dXO0FBTEg7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQVFFLHFFQUFDLDZDQUFEO0FBQU0sYUFBSyxFQUFDLFlBQVo7QUFBeUIsVUFBRSxFQUFFO0FBQUViLGtCQUFRLEVBQUUsQ0FBWjtBQUFlRyxpQkFBTyxFQUFFO0FBQXhCLFNBQTdCO0FBQUEsa0JBQ0dXO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBakJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBZ0NELENBdENEOztBQXdDZUYsZ0ZBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pEQTtBQUNBO0FBQ0E7O0FBV0EsTUFBTU0sY0FBdUMsR0FBRyxDQUFDO0FBQy9DVixPQUQrQztBQUUvQ1csVUFGK0M7QUFHL0NMLGFBSCtDO0FBSS9DTSxtQkFKK0M7QUFLL0MxQixTQUwrQztBQU0vQzJCO0FBTitDLENBQUQsS0FPMUM7QUFDSixzQkFDRSxxRUFBQyw2Q0FBRDtBQUNFLFdBQU8sRUFBRSxDQUFDLENBQUQsRUFBSSxTQUFKLENBRFg7QUFFRSxNQUFFLEVBQUUsRUFGTjtBQUdFLE1BQUUsRUFBRSxFQUhOO0FBSUUsS0FBQyxFQUFFLEtBSkw7QUFLRSxNQUFFLEVBQUMsT0FMTDtBQU1FLE1BQUUsRUFBRTtBQUNGekIsa0JBQVksRUFBRSxFQURaO0FBRUZvQixZQUFNLEVBQUUsU0FGTjtBQUdGRCxnQkFBVSxFQUFFLHlCQUhWO0FBSUZoQixlQUFTLEVBQUUsaUNBSlQ7QUFLRixpQkFBVztBQUNUa0IsaUJBQVMsRUFBRTtBQURGO0FBTFQsS0FOTjtBQWVFLFdBQU8sRUFBRXZCLE9BZlg7QUFBQSw0QkFpQkUscUVBQUMsOENBQUQ7QUFBTyxTQUFHLEVBQUVjLEtBQVo7QUFBbUIsYUFBTyxFQUFDO0FBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBakJGLGVBbUJFLHFFQUFDLDZDQUFEO0FBQU0sUUFBRSxFQUFDLEtBQVQ7QUFBZSxRQUFFLEVBQUU7QUFBRVgscUJBQWEsRUFBRTtBQUFqQixPQUFuQjtBQUFBLDhCQUNFLHFFQUFDLDZDQUFEO0FBQU0sYUFBSyxFQUFDLFVBQVo7QUFBdUIsVUFBRSxFQUFDLEtBQTFCO0FBQWdDLFVBQUUsRUFBRTtBQUFFRyxrQkFBUSxFQUFFLEVBQVo7QUFBZ0JFLG9CQUFVLEVBQUU7QUFBNUIsU0FBcEM7QUFBQSxrQkFDR2lCO0FBREg7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUlFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLFlBRFI7QUFFRSxVQUFFLEVBQUUsQ0FGTjtBQUdFLFVBQUUsRUFBQyxLQUhMO0FBSUUsVUFBRSxFQUFFO0FBQUVuQixrQkFBUSxFQUFFLENBQVo7QUFBZUcsaUJBQU8sRUFBRTtBQUF4QixTQUpOO0FBQUEsa0JBTUdXO0FBTkg7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFKRixlQVlFLHFFQUFDLDZDQUFEO0FBQU0sVUFBRSxFQUFFO0FBQUVWLG9CQUFVLEVBQUU7QUFBZCxTQUFWO0FBQW9DLGVBQU8sRUFBRWlCLGFBQTdDO0FBQUEsZ0NBQ0UscUVBQUMsNkNBQUQ7QUFBTSxlQUFLLEVBQUMsaUJBQVo7QUFBOEIsWUFBRSxFQUFFLEVBQWxDO0FBQXNDLFlBQUUsRUFBRTtBQUFFckIsb0JBQVEsRUFBRTtBQUFaLFdBQTFDO0FBQUEscUJBQ0dvQixpQkFESDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREYsZUFJRSxxRUFBQyx5REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFaRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBbkJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBeUNELENBakREOztBQW1EZUYsNkVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBU0EsTUFBTUksaUJBQTZDLEdBQUcsQ0FBQztBQUNyREMsT0FEcUQ7QUFFckRWLE1BRnFEO0FBR3JEQyxhQUhxRDtBQUlyRFUsTUFKcUQ7QUFLckQ5QjtBQUxxRCxDQUFELEtBTWhEO0FBQ0osc0JBQ0UscUVBQUMsNkNBQUQ7QUFDRSxRQUFJLEVBQUU4QixJQURSO0FBRUUsVUFBTSxFQUFDLFFBRlQ7QUFHRSxNQUFFLEVBQUMsT0FITDtBQUlFLE1BQUUsRUFBRSxFQUpOO0FBS0UsTUFBRSxFQUFFLEVBTE47QUFNRSxNQUFFLEVBQUU7QUFDRmYsWUFBTSxFQUFFLEdBRE47QUFHRmIsa0JBQVksRUFBRSxFQUhaO0FBSUZDLG1CQUFhLEVBQUUsUUFKYjtBQUtGa0IsZ0JBQVUsRUFBRSx5QkFMVjtBQU1GaEIsZUFBUyxFQUFFLG1DQU5UO0FBT0YsaUJBQVc7QUFDVGtCLGlCQUFTLEVBQUU7QUFERjtBQVBULEtBTk47QUFBQSw0QkFrQkU7QUFDRSxXQUFLLEVBQUU7QUFDTHJCLG9CQUFZLEVBQUUsRUFEVDtBQUVMNkIsY0FBTSxFQUFFLGVBRkg7QUFHTEMsY0FBTSxFQUFFO0FBSEgsT0FEVDtBQU1FLFNBQUcsRUFBRUgsS0FOUDtBQU9FLFlBQU0sRUFBRSxHQVBWO0FBUUUsV0FBSyxFQUFFO0FBUlQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFsQkYsZUE2QkUscUVBQUMsNkNBQUQ7QUFDRSxRQUFFLEVBQUUsRUFETjtBQUVFLGFBQU8sRUFBRTdCLE9BRlg7QUFHRSxRQUFFLEVBQUU7QUFBRUcscUJBQWEsRUFBRSxRQUFqQjtBQUEyQm1CLGNBQU0sRUFBRTtBQUFuQyxPQUhOO0FBQUEsOEJBS0UscUVBQUMsNkNBQUQ7QUFDRSxhQUFLLEVBQUMsVUFEUjtBQUVFLFVBQUUsRUFBQyxLQUZMO0FBR0UsVUFBRSxFQUFFO0FBQUVoQixrQkFBUSxFQUFFLEVBQVo7QUFBZ0JFLG9CQUFVLEVBQUU7QUFBNUIsU0FITjtBQUFBLGtCQUtHVztBQUxIO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBTEYsZUFZRSxxRUFBQyw2Q0FBRDtBQUFNLGFBQUssRUFBQyxZQUFaO0FBQXlCLFVBQUUsRUFBRTtBQUFFYixrQkFBUSxFQUFFLENBQVo7QUFBZUcsaUJBQU8sRUFBRTtBQUF4QixTQUE3QjtBQUFBLGtCQUNHVztBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBWkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQTdCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQWdERCxDQXZERDs7QUF5RGVRLGdGQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuRUE7QUFDQTtBQUNBOztBQUtBLE1BQU1LLE1BQXVCLEdBQUcsQ0FBQztBQUFFakM7QUFBRixDQUFELEtBQWlCO0FBQy9DLHNCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsTUFBRSxFQUFDLE9BREw7QUFFRSxNQUFFLEVBQUUsRUFGTjtBQUdFLE1BQUUsRUFBRTtBQUNGZSxZQUFNLEVBQUUsRUFETjtBQUVGWCxvQkFBYyxFQUFFLGVBRmQ7QUFHRk0sZ0JBQVUsRUFBRSxRQUhWO0FBSUZ3QixnQkFBVSxFQUFFO0FBSlYsS0FITjtBQUFBLDRCQVVFLHFFQUFDLDRDQUFEO0FBQUssUUFBRSxFQUFFLEVBQVQ7QUFBQSw2QkFDRSxxRUFBQyx5REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFWRixlQWNFLHFFQUFDLDZDQUFEO0FBQ0UsUUFBRSxFQUFFLEVBRE47QUFFRSxRQUFFLEVBQUU7QUFDRmxCLGFBQUssRUFBRSxHQURMO0FBRUZiLHFCQUFhLEVBQUUsUUFGYjtBQUdGQyxzQkFBYyxFQUFFO0FBSGQsT0FGTjtBQUFBLDhCQVFFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLFlBRFI7QUFFRSxVQUFFLEVBQUMsS0FGTDtBQUdFLFVBQUUsRUFBRTtBQUNGRSxrQkFBUSxFQUFFLENBRFI7QUFFRkUsb0JBQVUsRUFBRTtBQUZWLFNBSE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBUkYsZUFrQkUscUVBQUMsNkNBQUQ7QUFDRSxhQUFLLEVBQUMsWUFEUjtBQUVFLFVBQUUsRUFBRTtBQUNGRixrQkFBUSxFQUFFLENBRFI7QUFFRkcsaUJBQU8sRUFBRSxHQUZQO0FBR0ZGLG9CQUFVLEVBQUU7QUFIVixTQUZOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWxCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBZEYsZUEyQ0UscUVBQUMsNkNBQUQ7QUFBTSxVQUFJLEVBQUMsaUNBQVg7QUFBNkMsWUFBTSxFQUFDLFFBQXBEO0FBQUEsNkJBQ0UscUVBQUMsK0NBQUQ7QUFBUSxlQUFPLEVBQUVQLE9BQWpCO0FBQTBCLGVBQU8sRUFBQyxVQUFsQztBQUFBLCtCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsWUFBRSxFQUFFO0FBQ0ZNLG9CQUFRLEVBQUU7QUFEUixXQUROO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBM0NGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBeURELENBMUREOztBQTREZTJCLHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25FQTtBQUNBOztBQWNBLE1BQU1FLFNBQTZCLEdBQUcsQ0FBQztBQUFFQyxPQUFGO0FBQVNwQyxTQUFUO0FBQWtCcUM7QUFBbEIsQ0FBRCxLQUFzQztBQUMxRSxRQUFNO0FBQUEsT0FBQ0MsUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJDLHNEQUFRLENBQVlILFlBQVosYUFBWUEsWUFBWixjQUFZQSxZQUFaLEdBQTRCRCxLQUFLLENBQUMsQ0FBRCxDQUFqQyxDQUF4QztBQUNBSyx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJekMsT0FBSixFQUFhQSxPQUFPLENBQUNzQyxRQUFRLENBQUNJLEtBQVYsQ0FBUDtBQUNkLEdBRlEsRUFFTixDQUFDSixRQUFELENBRk0sQ0FBVDtBQUdBLHNCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsTUFBRSxFQUFDLE9BREw7QUFFRSxNQUFFLEVBQUUsRUFGTjtBQUdFLE1BQUUsRUFBRSxFQUhOO0FBSUUsS0FBQyxFQUFDLEtBSko7QUFLRSxNQUFFLEVBQUU7QUFDRnZCLFlBQU0sRUFBRSxFQUROO0FBRUZiLGtCQUFZLEVBQUUsRUFGWjtBQUdGRyxlQUFTLEVBQUU7QUFIVCxLQUxOO0FBQUEsY0FXRytCLEtBQUssQ0FBQ08sR0FBTixDQUFXQyxJQUFELGlCQUNULHFFQUFDLDZDQUFEO0FBRUUsYUFBTyxFQUFFLE1BQU07QUFDYkwsbUJBQVcsQ0FBQ0ssSUFBRCxDQUFYO0FBQ0QsT0FKSDtBQUtFLFFBQUUsRUFBRU4sUUFBUSxLQUFLTSxJQUFiLEdBQW9CLG1CQUFwQixHQUEwQyxPQUxoRDtBQU1FLFFBQUUsRUFBRTtBQUNGMUMsb0JBQVksRUFBRSxFQURaO0FBRUYyQyxZQUFJLEVBQUUsQ0FGSjtBQUdGekMsc0JBQWMsRUFBRSxRQUhkO0FBSUZNLGtCQUFVLEVBQUUsUUFKVjtBQUtGWSxjQUFNLEVBQUU7QUFMTixPQU5OO0FBQUEsNkJBY0UscUVBQUMsNkNBQUQ7QUFDRSxVQUFFLEVBQUU7QUFDRmQsb0JBQVUsRUFBRThCLFFBQVEsS0FBS00sSUFBYixHQUFvQixLQUFwQixHQUE0QixNQUR0QztBQUVGdEMsa0JBQVEsRUFBRSxFQUZSO0FBR0ZDLG9CQUFVLEVBQUU7QUFIVixTQUROO0FBTUUsYUFBSyxFQUFFK0IsUUFBUSxLQUFLTSxJQUFiLEdBQW9CLGlCQUFwQixHQUF3QyxVQU5qRDtBQUFBLGtCQVFHQSxJQUFJLENBQUMvQjtBQVJSO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkRixPQUNPK0IsSUFBSSxDQUFDRSxFQURaO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREQ7QUFYSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUF5Q0QsQ0E5Q0Q7O0FBZ0RlWCx3RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNWSxLQUFLLEdBQUcsQ0FDWjtBQUNFRCxJQUFFLEVBQUUsQ0FETjtBQUVFakMsT0FBSyxFQUFFLFlBRlQ7QUFHRTZCLE9BQUssRUFBRTtBQUhULENBRFksRUFNWjtBQUNFSSxJQUFFLEVBQUUsQ0FETjtBQUVFakMsT0FBSyxFQUFFLFVBRlQ7QUFHRTZCLE9BQUssRUFBRTtBQUhULENBTlksRUFXWjtBQUNFSSxJQUFFLEVBQUUsQ0FETjtBQUVFakMsT0FBSyxFQUFFLFdBRlQ7QUFHRTZCLE9BQUssRUFBRTtBQUhULENBWFksQ0FBZDtBQWtCQSxNQUFNTSxRQUFRLEdBQUcsQ0FDZjtBQUNFbEMsT0FBSyxFQUFFLG1CQURUO0FBRUVXLFVBQVEsRUFBRSxpQkFGWjtBQUdFTCxhQUFXLEVBQUUsMkRBSGY7QUFJRU0sbUJBQWlCLEVBQUU7QUFKckIsQ0FEZSxFQU9mO0FBQ0VaLE9BQUssRUFBRSxtQkFEVDtBQUVFVyxVQUFRLEVBQUUscUJBRlo7QUFHRUwsYUFBVyxFQUFFLGdEQUhmO0FBSUVNLG1CQUFpQixFQUFFO0FBSnJCLENBUGUsRUFhZjtBQUNFWixPQUFLLEVBQUUsbUJBRFQ7QUFFRVcsVUFBUSxFQUFFLDBCQUZaO0FBR0VMLGFBQVcsRUFBRSx3REFIZjtBQUlFTSxtQkFBaUIsRUFBRTtBQUpyQixDQWJlLEVBbUJmO0FBQ0VaLE9BQUssRUFBRSxtQkFEVDtBQUVFVyxVQUFRLEVBQUUsd0JBRlo7QUFHRUwsYUFBVyxFQUFFLHdCQUhmO0FBSUVNLG1CQUFpQixFQUFFO0FBSnJCLENBbkJlLENBQWpCOztBQTJCQSxNQUFNdUIsU0FBYSxHQUFHLE1BQU07QUFDMUIsUUFBTUMsTUFBTSxHQUFHQyw2REFBUyxFQUF4QjtBQUNBLFFBQU07QUFBQSxPQUFDQyxLQUFEO0FBQUEsT0FBUUM7QUFBUixNQUFvQmIsc0RBQVEsQ0FBQyxLQUFELENBQWxDO0FBQ0EsUUFBTTtBQUFBLE9BQUNjLEdBQUQ7QUFBQSxPQUFNQztBQUFOLE1BQWdCZixzREFBUSxDQUFDLEdBQUQsQ0FBOUI7O0FBRUEsUUFBTWdCLFdBQVcsR0FBRyxNQUFNO0FBQ3hCLFFBQUksQ0FBQ0osS0FBTCxFQUFZQyxRQUFRLENBQUMsSUFBRCxDQUFSO0FBQ2IsR0FGRDs7QUFJQSxzQkFDRSxxRUFBQyw2Q0FBRDtBQUNFLE1BQUUsRUFBRTtBQUNGckMsV0FBSyxFQUFFLE1BREw7QUFFRlosb0JBQWMsRUFBRSxRQUZkO0FBR0ZNLGdCQUFVLEVBQUU7QUFIVixLQUROO0FBQUEsMkJBT0UscUVBQUMsNkNBQUQ7QUFDRSxRQUFFLEVBQUMsU0FETDtBQUVFLFFBQUUsRUFBRTtBQUNGTSxhQUFLLEVBQUUsR0FETDtBQUVGRCxjQUFNLEVBQUUsR0FGTjtBQUdGWixxQkFBYSxFQUFFLFFBSGI7QUFJRnNELGlCQUFTLEVBQUU7QUFKVCxPQUZOO0FBQUEsOEJBU0UscUVBQUMsNkNBQUQ7QUFDRSxVQUFFLEVBQUMsU0FETDtBQUVFLFVBQUUsRUFBRTtBQUNGMUMsZ0JBQU0sRUFBRXFDLEtBQUssR0FBRyxFQUFILEdBQVEsR0FEbkI7QUFFRnBDLGVBQUssRUFBRTtBQUZMLFNBRk47QUFBQSwrQkFPRSxxRUFBQyw2Q0FBRDtBQUNFLFlBQUUsRUFBQyxTQURMO0FBRUUsWUFBRSxFQUFFLEVBRk47QUFHRSxZQUFFLEVBQUUsRUFITjtBQUlFLFlBQUUsRUFBRTtBQUNGQSxpQkFBSyxFQUFFLE1BREw7QUFFRkQsa0JBQU0sRUFBRSxFQUZOO0FBR0ZYLDBCQUFjLEVBQUUsZUFIZDtBQUlGTSxzQkFBVSxFQUFFLFFBSlY7QUFLRmdELGtCQUFNLEVBQUU7QUFMTixXQUpOO0FBQUEsa0NBWUUscUVBQUMsNkNBQUQ7QUFDRSxjQUFFLEVBQUU7QUFDRnBDLG9CQUFNLEVBQUUsU0FETjtBQUVGRCx3QkFBVSxFQUFFLHlCQUZWO0FBR0YseUJBQVc7QUFDVEUseUJBQVMsRUFBRTtBQURGO0FBSFQsYUFETjtBQUFBLG1DQVNFLHFFQUFDLHlEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFURjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQVpGLGVBdUJFLHFFQUFDLDZDQUFEO0FBQ0UsbUJBQU8sRUFBRSxNQUFNO0FBQ2IyQixvQkFBTSxDQUFDUyxNQUFQO0FBQ0QsYUFISDtBQUlFLGNBQUUsRUFBRTtBQUNGckMsb0JBQU0sRUFBRSxTQUROO0FBRUZELHdCQUFVLEVBQUUseUJBRlY7QUFHRix5QkFBVztBQUNURSx5QkFBUyxFQUFFO0FBREY7QUFIVCxhQUpOO0FBQUEsbUNBWUUscUVBQUMsOENBQUQ7QUFBTyxpQkFBRyxFQUFDLG9CQUFYO0FBQWdDLHFCQUFPLEVBQUM7QUFBeEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVpGO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBdkJGLGVBcUNFLHFFQUFDLDZDQUFEO0FBQ0UsY0FBRSxFQUFFO0FBQ0ZELG9CQUFNLEVBQUUsU0FETjtBQUVGRCx3QkFBVSxFQUFFLHlCQUZWO0FBR0YseUJBQVc7QUFDVEUseUJBQVMsRUFBRTtBQURGO0FBSFQsYUFETjtBQUFBLG1DQVNFLHFFQUFDLDBEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFURjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQXJDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQRjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVRGLEVBa0VHLENBQUM2QixLQUFELGlCQUNDLHFFQUFDLDZDQUFEO0FBQ0UsVUFBRSxFQUFFLENBQUMsR0FEUDtBQUVFLFVBQUUsRUFBRTtBQUNGTSxnQkFBTSxFQUFFO0FBRE4sU0FGTjtBQUFBLCtCQU1FLHFFQUFDLDhEQUFEO0FBQ0UsZUFBSyxFQUFDLHNEQURSO0FBRUUsaUJBQU8sRUFBQywrREFGVjtBQUdFLGlCQUFPLEVBQUVGO0FBSFg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU5GO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBbkVKLGVBZ0ZFLHFFQUFDLDZDQUFEO0FBQ0UsVUFBRSxFQUFFLENBQUMsR0FEUDtBQUVFLFVBQUUsRUFBRTtBQUNGekMsZ0JBQU0sRUFBRSxHQUROO0FBRUZDLGVBQUssRUFBRTtBQUZMLFNBRk47QUFBQSwrQkFPRSxxRUFBQyw2Q0FBRDtBQUNFLFlBQUUsRUFBRW9DLEtBQUssR0FBRyxHQUFILEdBQVMsR0FEcEI7QUFFRSxZQUFFLEVBQUU7QUFDRnBDLGlCQUFLLEVBQUUsTUFETDtBQUVGYix5QkFBYSxFQUFFO0FBRmIsV0FGTjtBQUFBLGtDQU9FLHFFQUFDLDZDQUFEO0FBQ0UsY0FBRSxFQUFFO0FBQ0ZhLG1CQUFLLEVBQUUsTUFETDtBQUVGYiwyQkFBYSxFQUFFO0FBRmIsYUFETjtBQUFBLG9DQU1FLHFFQUFDLDhEQUFEO0FBQ0UsbUJBQUssRUFBRTRDLEtBRFQ7QUFFRSxxQkFBTyxFQUFHTCxLQUFELElBQVc7QUFDbEJhLHNCQUFNLENBQUNiLEtBQUQsQ0FBTjtBQUNBLG9CQUFJLENBQUNVLEtBQUQsSUFBVVYsS0FBSyxLQUFLLEdBQXhCLEVBQTZCVyxRQUFRLENBQUMsSUFBRCxDQUFSO0FBQzlCLGVBTEg7QUFNRSwwQkFBWSxFQUFFTixLQUFLLENBQUMsQ0FBRDtBQU5yQjtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQU5GLEVBY0dPLEdBQUcsS0FBSyxHQUFSLGdCQUNDLHFFQUFDLDZDQUFEO0FBQ0UsZ0JBQUUsRUFBRTtBQUNGbkQsNkJBQWEsRUFBRSxRQURiO0FBRUZhLHFCQUFLLEVBQUU7QUFGTCxlQUROO0FBQUEsd0JBTUdnQyxRQUFRLENBQUNMLEdBQVQsQ0FBYSxDQUFDQyxJQUFELEVBQU9nQixLQUFQLGtCQUNaLHFFQUFDLDZDQUFEO0FBQUEsdUNBQ0UscUVBQUMsa0VBQUQ7QUFDRSx5QkFBTyxFQUFFSixXQURYO0FBRUUsdUJBQUssRUFBRVosSUFBSSxDQUFDOUIsS0FGZDtBQUdFLDBCQUFRLEVBQUU4QixJQUFJLENBQUNuQixRQUhqQjtBQUlFLDZCQUFXLEVBQUVtQixJQUFJLENBQUN4QixXQUpwQjtBQUtFLG1DQUFpQixFQUFFd0IsSUFBSSxDQUFDbEI7QUFMMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLGlCQUFXa0MsS0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQUREO0FBTkg7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERCxHQW1CR04sR0FBRyxLQUFLLEdBQVIsZ0JBQ0YscUVBQUMsNkNBQUQ7QUFDRSxnQkFBRSxFQUFFO0FBQ0ZuRCw2QkFBYSxFQUFFLFFBRGI7QUFFRmEscUJBQUssRUFBRTtBQUZMLGVBRE47QUFBQSxzQ0FNRSxxRUFBQyw2Q0FBRDtBQUFBLHVDQUNFLHFFQUFDLHFFQUFEO0FBQ0Usc0JBQUksRUFBQyw2Q0FEUDtBQUVFLHVCQUFLLEVBQUMsMkNBRlI7QUFHRSxzQkFBSSxFQUFDLHFDQUhQO0FBSUUsNkJBQVcsRUFBQztBQUpkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQU5GLGVBY0UscUVBQUMsNkNBQUQ7QUFBTSx1QkFBTyxFQUFFLENBQWY7QUFBa0Isa0JBQUUsRUFBRSxFQUF0QjtBQUFBLHdDQUNFLHFFQUFDLDZDQUFEO0FBQUEseUNBQ0UscUVBQUMscUVBQUQ7QUFDRSx5QkFBSyxFQUFDLG9CQURSO0FBRUUsd0JBQUksRUFBQyxPQUZQO0FBR0UsK0JBQVcsRUFBQztBQUhkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZCQURGLGVBUUUscUVBQUMsNkNBQUQ7QUFBQSx5Q0FDRSxxRUFBQyxxRUFBRDtBQUNFLHlCQUFLLEVBQUMsbUJBRFI7QUFFRSx3QkFBSSxFQUFDLE9BRlA7QUFHRSwrQkFBVyxFQUFDO0FBSGQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBUkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQWRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERSxnQkFpQ0YscUVBQUMsNkNBQUQ7QUFDRSxnQkFBRSxFQUFFO0FBQ0ZiLDZCQUFhLEVBQUUsUUFEYjtBQUVGTywwQkFBVSxFQUFFO0FBRlYsZUFETjtBQUFBLHNDQU1FLHFFQUFDLDZDQUFEO0FBQU0sa0JBQUUsRUFBRTtBQUFFTSx1QkFBSyxFQUFFO0FBQVQsaUJBQVY7QUFBQSx1Q0FDRSxxRUFBQyxpRUFBRDtBQUNFLHVCQUFLLEVBQUMsdUJBRFI7QUFFRSx1QkFBSyxFQUFDLHNCQUZSO0FBR0UseUJBQU8sRUFBQztBQUhWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQU5GLGVBYUUscUVBQUMsNkNBQUQ7QUFDRSxxQkFBSyxFQUFDLFVBRFI7QUFFRSxrQkFBRSxFQUFFLEVBRk47QUFHRSxrQkFBRSxFQUFDLEtBSEw7QUFJRSxrQkFBRSxFQUFFO0FBQ0ZWLDBCQUFRLEVBQUUsRUFEUjtBQUVGRSw0QkFBVSxFQUFFO0FBRlYsaUJBSk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBYkYsZUF3QkUscUVBQUMsK0NBQUQ7QUFBUSx1QkFBTyxFQUFDLE9BQWhCO0FBQUEsdUNBQ0UscUVBQUMsNkNBQUQ7QUFDRSxvQkFBRSxFQUFFO0FBQ0ZKLGtDQUFjLEVBQUUsUUFEZDtBQUVGTSw4QkFBVSxFQUFFO0FBRlYsbUJBRE47QUFBQSwwQ0FNRSxxRUFBQywwREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLCtCQU5GLGVBT0UscUVBQUMsNkNBQUQ7QUFBTSxzQkFBRSxFQUFFLEVBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsK0JBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkF4QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQWxFSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBUEYsZUErR0UscUVBQUMsNkNBQUQ7QUFDRSxjQUFFLEVBQUU7QUFDRlQsc0JBQVEsRUFBRW1ELEtBQUssR0FBRyxPQUFILEdBQWEsVUFEMUI7QUFFRlMsaUJBQUcsRUFBRVQsS0FBSyxHQUFHLEdBQUgsR0FBUyxDQUZqQjtBQUdGcEMsbUJBQUssRUFBRW9DLEtBQUssR0FBRyxHQUFILEdBQVM7QUFIbkIsYUFETjtBQUFBLG1DQU9FLHFFQUFDLDBEQUFEO0FBQVEscUJBQU8sRUFBRUk7QUFBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBL0dGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBaEZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQTRORCxDQXJPRDs7QUF1T2VQLHdFQUFmLEU7Ozs7Ozs7Ozs7OztBQ2xTQTtBQUFBO0FBQUE7QUFBQTs7QUFFQSxxQkFBcUIsZ0RBQWdELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZSxHQUFHLHdDQUF3Qzs7QUFFNVI7O0FBRS9CO0FBQ0Esc0JBQXNCLG1EQUFtQjtBQUN6QztBQUNBO0FBQ0E7QUFDQSxHQUFHLHlDQUF5QyxtREFBbUI7QUFDL0Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVlLHVFQUFRLEU7Ozs7Ozs7Ozs7OztBQ2xCdkI7QUFBQTtBQUFBO0FBQUE7O0FBRUEscUJBQXFCLGdEQUFnRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWUsR0FBRyx3Q0FBd0M7O0FBRTVSOztBQUUvQjtBQUNBLHNCQUFzQixtREFBbUI7QUFDekM7QUFDQTtBQUNBO0FBQ0EsR0FBRyx5Q0FBeUMsbURBQW1CO0FBQy9EO0FBQ0E7QUFDQTtBQUNBLEdBQUcscUNBQXFDLG1EQUFtQjtBQUMzRDtBQUNBO0FBQ0E7QUFDQSxHQUFHLHFDQUFxQyxtREFBbUI7QUFDM0Q7QUFDQTtBQUNBO0FBQ0EsR0FBRyxxQ0FBcUMsbURBQW1CO0FBQzNEO0FBQ0E7QUFDQTtBQUNBLEdBQUcscUNBQXFDLG1EQUFtQjtBQUMzRDtBQUNBO0FBQ0E7QUFDQSxHQUFHLHFDQUFxQyxtREFBbUI7QUFDM0Q7QUFDQTtBQUNBO0FBQ0EsR0FBRyxxQ0FBcUMsbURBQW1CO0FBQzNEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFZSx1RUFBUSxFOzs7Ozs7Ozs7Ozs7QUMxQ3ZCO0FBQUE7QUFBQTtBQUFBOztBQUVBLHFCQUFxQixnREFBZ0QsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlLEdBQUcsd0NBQXdDOztBQUU1Ujs7QUFFL0I7QUFDQSxzQkFBc0IsbURBQW1CO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLEdBQUcsbUNBQW1DLG1EQUFtQjtBQUN6RDtBQUNBLEdBQUcsZUFBZSxtREFBbUI7QUFDckM7QUFDQSxHQUFHLGVBQWUsbURBQW1CO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLGlDQUFpQyxtREFBbUI7QUFDdkQ7QUFDQSxHQUFHLGVBQWUsbURBQW1CO0FBQ3JDO0FBQ0EsR0FBRyxlQUFlLG1EQUFtQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVlLHVFQUFRLEU7Ozs7Ozs7Ozs7OztBQ2xDdkI7QUFBQTtBQUFBO0FBQUE7O0FBRUEscUJBQXFCLGdEQUFnRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWUsR0FBRyx3Q0FBd0M7O0FBRTVSOztBQUUvQjtBQUNBLHNCQUFzQixtREFBbUI7QUFDekM7QUFDQTtBQUNBO0FBQ0EsR0FBRyxtQ0FBbUMsbURBQW1CO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBLEdBQUcsZUFBZSxtREFBbUI7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsZ0JBQWdCLG1EQUFtQjtBQUN0QztBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVlLHNFQUFPLEU7Ozs7Ozs7Ozs7OztBQzNCdEI7QUFBQTtBQUFBO0FBQUE7O0FBRUEscUJBQXFCLGdEQUFnRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWUsR0FBRyx3Q0FBd0M7O0FBRTVSOztBQUUvQjtBQUNBLHNCQUFzQixtREFBbUI7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLHlDQUF5QyxtREFBbUI7QUFDL0Q7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFZSx1RUFBUSxFOzs7Ozs7Ozs7Ozs7QUNsQnZCO0FBQUE7QUFBQTtBQUFBOztBQUVBLHFCQUFxQixnREFBZ0QsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlLEdBQUcsd0NBQXdDOztBQUU1Ujs7QUFFL0I7QUFDQSxzQkFBc0IsbURBQW1CO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLEdBQUcsbUNBQW1DLG1EQUFtQjtBQUN6RDtBQUNBLEdBQUcsZUFBZSxtREFBbUI7QUFDckM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVlLDRFQUFhLEU7Ozs7Ozs7Ozs7O0FDcEI1Qix3Qzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxxQyIsImZpbGUiOiJwYWdlcy9kYXNoYm9hcmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3BhZ2VzL2Rhc2hib2FyZC50c3hcIik7XG4iLCJpbXBvcnQgUmVhY3QsIHsgRkMgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgQnV0dG9uLCBGbGV4LCBUZXh0LCBJbWFnZSwgTGluayB9IGZyb20gXCJ0aGVtZS11aVwiO1xyXG5pbXBvcnQgV2hpdGVBcnJvd0ljb24gZnJvbSBcIi4uL3B1YmxpYy93aGl0ZUFycm93LnN2Z1wiO1xyXG5cclxuaW50ZXJmYWNlIENhcmRCdXR0b25Qcm9wcyB7XHJcbiAgdGl0bGU6IHN0cmluZztcclxuICBjb250ZW50OiBzdHJpbmc7XHJcbiAgb25DbGljaz86ICgpID0+IHZvaWQ7XHJcbn1cclxuXHJcbmNvbnN0IENhcmRCdXR0b246IEZDPENhcmRCdXR0b25Qcm9wcz4gPSAoeyB0aXRsZSwgY29udGVudCwgb25DbGljayB9KSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxGbGV4XHJcbiAgICAgIGJnPVwid2hpdGVcIlxyXG4gICAgICBteD17MTV9XHJcbiAgICAgIHN4PXt7XHJcbiAgICAgICAgcG9zaXRpb246IFwicmVsYXRpdmVcIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6IDE2LFxyXG4gICAgICAgIGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIsXHJcbiAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwic3BhY2UtYmV0d2VlblwiLFxyXG4gICAgICAgIGJveFNoYWRvdzogXCJyZ2IoMCAwIDAgLyA1JSkgMXB4IDhweCA4cHggMXB4XCIsXHJcbiAgICAgIH19XHJcbiAgICA+XHJcbiAgICAgIDxUZXh0XHJcbiAgICAgICAgbXk9ezE4fVxyXG4gICAgICAgIG1sPXsyMH1cclxuICAgICAgICBtcj17ODB9XHJcbiAgICAgICAgY29sb3I9XCIjNEY0RjdEXCJcclxuICAgICAgICBzeD17e1xyXG4gICAgICAgICAgZm9udFNpemU6IDIwLFxyXG4gICAgICAgICAgbGluZUhlaWdodDogXCIzMHB4XCIsXHJcbiAgICAgICAgICBmb250V2VpZ2h0OiBcImhlYWRpbmdcIixcclxuICAgICAgICB9fVxyXG4gICAgICA+XHJcbiAgICAgICAge3RpdGxlfVxyXG4gICAgICA8L1RleHQ+XHJcbiAgICAgIDxUZXh0XHJcbiAgICAgICAgY29sb3I9XCIjNEY0RjdEXCJcclxuICAgICAgICBteD17MjB9XHJcbiAgICAgICAgc3g9e3tcclxuICAgICAgICAgIGZvbnRTaXplOiAxLFxyXG5cclxuICAgICAgICAgIGZvbnRXZWlnaHQ6IFwiNjAwXCIsXHJcbiAgICAgICAgICBvcGFjaXR5OiAwLjYsXHJcbiAgICAgICAgfX1cclxuICAgICAgPlxyXG4gICAgICAgIHtjb250ZW50fVxyXG4gICAgICA8L1RleHQ+XHJcblxyXG4gICAgICA8RmxleFxyXG4gICAgICAgIG14PXsyMH1cclxuICAgICAgICBzeD17e1xyXG4gICAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwic3BhY2UtYmV0d2VlblwiLFxyXG4gICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcclxuICAgICAgICB9fVxyXG4gICAgICA+XHJcbiAgICAgICAgPExpbmtcclxuICAgICAgICAgIGhyZWY9XCJodHRwczovL3R1b2l0cmUudm4vdGltLWtpZW0uaHRtP2tleXdvcmRzPWNvdmlkXCJcclxuICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPEJ1dHRvbiBvbkNsaWNrPXtvbkNsaWNrfSB2YXJpYW50PVwic2Vjb25kYXJ5XCI+XHJcbiAgICAgICAgICAgIDxUZXh0XHJcbiAgICAgICAgICAgICAgbXI9ezIwfVxyXG4gICAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgICBmb250U2l6ZTogMTQsXHJcbiAgICAgICAgICAgICAgICBjb2xvcjogXCJ3aGl0ZVwiLFxyXG4gICAgICAgICAgICAgICAgZm9udFdlaWdodDogXCI2MDBcIixcclxuICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgU2VlIE1vcmVcclxuICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICA8V2hpdGVBcnJvd0ljb24gLz5cclxuICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgIDwvTGluaz5cclxuICAgICAgICA8SW1hZ2Ugc3JjPVwiL2ltYWdlcy9wZXJzb24uUE5HXCIgdmFyaWFudD1cInBlcnNvblwiIC8+XHJcbiAgICAgIDwvRmxleD5cclxuICAgIDwvRmxleD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgQ2FyZEJ1dHRvbjtcclxuIiwiaW1wb3J0IFJlYWN0LCB7IEZDIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IEZsZXgsIFRleHQsIEltYWdlIH0gZnJvbSBcInRoZW1lLXVpXCI7XHJcblxyXG5pbnRlcmZhY2UgQ2FyZERpYWdub3Npc1Byb3BzIHtcclxuICBsYWJlbDogc3RyaW5nO1xyXG4gIGNvbnRlbnQ6IHN0cmluZztcclxuICBpbWFnZTogc3RyaW5nO1xyXG59XHJcblxyXG5jb25zdCBDYXJkRGlhZ25vc2lzOiBGQzxDYXJkRGlhZ25vc2lzUHJvcHM+ID0gKHsgbGFiZWwsIGNvbnRlbnQsIGltYWdlIH0pID0+IHtcclxuICByZXR1cm4gKFxyXG4gICAgPEZsZXhcclxuICAgICAgYmc9XCJ3aGl0ZVwiXHJcbiAgICAgIG14PXsxNX1cclxuICAgICAgbWI9ezEwfVxyXG4gICAgICBzeD17e1xyXG4gICAgICAgIGhlaWdodDogMzAwLFxyXG4gICAgICAgIHdpZHRoOiAzNjAsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAxNixcclxuICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgYm94U2hhZG93OiBcInJnYigwIDAgMCAvIDUlKSAxcHggMTBweCAxMHB4IDFweFwiLFxyXG4gICAgICB9fVxyXG4gICAgPlxyXG4gICAgICA8SW1hZ2Ugc3JjPXtpbWFnZX0gdmFyaWFudD1cImRpYWdub3Npc1wiIC8+XHJcbiAgICAgIDxUZXh0XHJcbiAgICAgICAgbXg9ezE1fVxyXG4gICAgICAgIGNvbG9yPVwiYmlnU3RvbmVcIlxyXG4gICAgICAgIG1iPVwiNXB4XCJcclxuICAgICAgICBzeD17eyBmb250U2l6ZTogMTUsIGZvbnRXZWlnaHQ6IFwiaGVhZGluZ1wiIH19XHJcbiAgICAgID5cclxuICAgICAgICB7bGFiZWx9XHJcbiAgICAgIDwvVGV4dD5cclxuICAgICAgPFRleHRcclxuICAgICAgICBtdD17MTB9XHJcbiAgICAgICAgbXg9ezE1fVxyXG4gICAgICAgIGNvbG9yPVwib3V0ZXJTcGFjZVwiXHJcbiAgICAgICAgc3g9e3sgZm9udFNpemU6IDAsIG9wYWNpdHk6IDAuNiwgdGV4dEFsaWduOiBcImNlbnRlclwiLCB3aWR0aDogMjUwIH19XHJcbiAgICAgID5cclxuICAgICAgICB7Y29udGVudH1cclxuICAgICAgPC9UZXh0PlxyXG4gICAgPC9GbGV4PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDYXJkRGlhZ25vc2lzO1xyXG4iLCJpbXBvcnQgUmVhY3QsIHsgRkMgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgRmxleCwgVGV4dCwgSW1hZ2UgfSBmcm9tIFwidGhlbWUtdWlcIjtcclxuXHJcbmludGVyZmFjZSBDYXJkSW1hZ2VTeW1wdG9tc1Byb3BzIHtcclxuICBpbWFnZTogc3RyaW5nO1xyXG4gIG5hbWU6IHN0cmluZztcclxuICBkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gIG9uQ2xpY2s/OiAoKSA9PiB2b2lkO1xyXG59XHJcbmNvbnN0IENhcmRJbWFnZVN5bXB0b21zOiBGQzxDYXJkSW1hZ2VTeW1wdG9tc1Byb3BzPiA9ICh7XHJcbiAgaW1hZ2UsXHJcbiAgbmFtZSxcclxuICBkZXNjcmlwdGlvbixcclxuICBvbkNsaWNrLFxyXG59KSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxGbGV4XHJcbiAgICAgIG9uQ2xpY2s9e29uQ2xpY2t9XHJcbiAgICAgIGJnPVwid2hpdGVcIlxyXG4gICAgICBzeD17e1xyXG4gICAgICAgIGhlaWdodDogMjE1LFxyXG4gICAgICAgIGJvcmRlclJhZGl1czogMTYsXHJcbiAgICAgICAgZmxleERpcmVjdGlvbjogXCJjb2x1bW5cIixcclxuICAgICAgICB0cmFuc2l0aW9uOiBcImFsbCAwLjNzIGVhc2UtaW4tb3V0IDBzXCIsXHJcbiAgICAgICAgYm94U2hhZG93OiBcInJnYigwIDAgMCAvIDUlKSAxcHggMTBweCAxMHB4IDFweFwiLFxyXG4gICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICAgIHRyYW5zZm9ybTogXCJzY2FsZSgwLjk1KVwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH19XHJcbiAgICA+XHJcbiAgICAgIDxJbWFnZSBzcmM9e2ltYWdlfSB2YXJpYW50PVwic3ltcHRvbXNcIiAvPlxyXG5cclxuICAgICAgPEZsZXggbXg9ezE1fSBzeD17eyBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiIH19PlxyXG4gICAgICAgIDxUZXh0XHJcbiAgICAgICAgICBjb2xvcj1cImJpZ1N0b25lXCJcclxuICAgICAgICAgIG1iPVwiM3B4XCJcclxuICAgICAgICAgIHN4PXt7IGZvbnRTaXplOiAxLCBmb250V2VpZ2h0OiBcImhlYWRpbmdcIiB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIHtuYW1lfVxyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgICA8VGV4dCBjb2xvcj1cIm91dGVyU3BhY2VcIiBzeD17eyBmb250U2l6ZTogMCwgb3BhY2l0eTogMC42IH19PlxyXG4gICAgICAgICAge2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgPC9GbGV4PlxyXG4gICAgPC9GbGV4PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDYXJkSW1hZ2VTeW1wdG9tcztcclxuIiwiaW1wb3J0IFJlYWN0LCB7IEZDIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IEZsZXgsIEltYWdlLCBUZXh0LCBHcmlkIH0gZnJvbSBcInRoZW1lLXVpXCI7XHJcbmltcG9ydCBBcnJvd0ljb24gZnJvbSBcIi4uL3B1YmxpYy9hcnJvdy5zdmdcIjtcclxuXHJcbmludGVyZmFjZSBDYXJkUHJldmVudGlvblByb3BzIHtcclxuICBpbWFnZTogc3RyaW5nO1xyXG4gIG5hbWVDYXJkOiBzdHJpbmc7XHJcbiAgZGVzY3JpcHRpb246IHN0cmluZztcclxuICBudW1iZXJEaXNjdXNzaW9uczogc3RyaW5nO1xyXG4gIG9uQ2xpY2s/OiAoKSA9PiB2b2lkO1xyXG4gIG9uRGlzY3Vzc2lvbnM/OiAoKSA9PiB2b2lkO1xyXG59XHJcblxyXG5jb25zdCBDYXJkUHJldmVudGlvbjogRkM8Q2FyZFByZXZlbnRpb25Qcm9wcz4gPSAoe1xyXG4gIGltYWdlLFxyXG4gIG5hbWVDYXJkLFxyXG4gIGRlc2NyaXB0aW9uLFxyXG4gIG51bWJlckRpc2N1c3Npb25zLFxyXG4gIG9uQ2xpY2ssXHJcbiAgb25EaXNjdXNzaW9ucyxcclxufSkgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICA8R3JpZFxyXG4gICAgICBjb2x1bW5zPXtbMiwgXCIxZnIgMmZyXCJdfVxyXG4gICAgICBteD17MTV9XHJcbiAgICAgIG1iPXsxMH1cclxuICAgICAgcD17XCI3cHhcIn1cclxuICAgICAgYmc9XCJ3aGl0ZVwiXHJcbiAgICAgIHN4PXt7XHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAxNixcclxuICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxyXG4gICAgICAgIHRyYW5zaXRpb246IFwiYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHNcIixcclxuICAgICAgICBib3hTaGFkb3c6IFwicmdiKDAgMCAwIC8gNSUpIDFweCA4cHggOHB4IDFweFwiLFxyXG4gICAgICAgIFwiOmFjdGl2ZVwiOiB7XHJcbiAgICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45NSlcIixcclxuICAgICAgICB9LFxyXG4gICAgICB9fVxyXG4gICAgICBvbkNsaWNrPXtvbkNsaWNrfVxyXG4gICAgPlxyXG4gICAgICA8SW1hZ2Ugc3JjPXtpbWFnZX0gdmFyaWFudD1cImF2YXRhclwiIC8+XHJcblxyXG4gICAgICA8RmxleCBteT1cIjdweFwiIHN4PXt7IGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIgfX0+XHJcbiAgICAgICAgPFRleHQgY29sb3I9XCJiaWdTdG9uZVwiIG1iPVwiNXB4XCIgc3g9e3sgZm9udFNpemU6IDE1LCBmb250V2VpZ2h0OiA3MDAgfX0+XHJcbiAgICAgICAgICB7bmFtZUNhcmR9XHJcbiAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgIDxUZXh0XHJcbiAgICAgICAgICBjb2xvcj1cIm91dGVyU3BhY2VcIlxyXG4gICAgICAgICAgbWI9ezl9XHJcbiAgICAgICAgICBtcj1cIjVweFwiXHJcbiAgICAgICAgICBzeD17eyBmb250U2l6ZTogMCwgb3BhY2l0eTogMC42IH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAge2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgICA8RmxleCBzeD17eyBhbGlnbkl0ZW1zOiBcImNlbnRlclwiIH19IG9uQ2xpY2s9e29uRGlzY3Vzc2lvbnN9PlxyXG4gICAgICAgICAgPFRleHQgY29sb3I9XCJjaGVsc2VhQ3VjdW1iZXJcIiBtcj17MTV9IHN4PXt7IGZvbnRTaXplOiAxNCB9fT5cclxuICAgICAgICAgICAge251bWJlckRpc2N1c3Npb25zfSBEaXNjdXNzaW9uc1xyXG4gICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgPEFycm93SWNvbiAvPlxyXG4gICAgICAgIDwvRmxleD5cclxuICAgICAgPC9GbGV4PlxyXG4gICAgPC9HcmlkPlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDYXJkUHJldmVudGlvbjtcclxuIiwiaW1wb3J0IFJlYWN0LCB7IEZDIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IEZsZXgsIFRleHQsIExpbmsgfSBmcm9tIFwidGhlbWUtdWlcIjtcclxuXHJcbmludGVyZmFjZSBDYXJkVmlkZW9TeW1wdG9tc1Byb3BzIHtcclxuICB2aWRlbzogc3RyaW5nO1xyXG4gIG5hbWU6IHN0cmluZztcclxuICBkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gIGxpbms6IHN0cmluZztcclxuICBvbkNsaWNrPzogKCkgPT4gdm9pZDtcclxufVxyXG5jb25zdCBDYXJkVmlkZW9TeW1wdG9tczogRkM8Q2FyZFZpZGVvU3ltcHRvbXNQcm9wcz4gPSAoe1xyXG4gIHZpZGVvLFxyXG4gIG5hbWUsXHJcbiAgZGVzY3JpcHRpb24sXHJcbiAgbGluayxcclxuICBvbkNsaWNrLFxyXG59KSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxMaW5rXHJcbiAgICAgIGhyZWY9e2xpbmt9XHJcbiAgICAgIHRhcmdldD1cIl9ibGFua1wiXHJcbiAgICAgIGJnPVwid2hpdGVcIlxyXG4gICAgICBteD17MTV9XHJcbiAgICAgIG1iPXsxMH1cclxuICAgICAgc3g9e3tcclxuICAgICAgICBoZWlnaHQ6IDI3MCxcclxuXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAxNixcclxuICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgIHRyYW5zaXRpb246IFwiYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHNcIixcclxuICAgICAgICBib3hTaGFkb3c6IFwicmdiKDAgMCAwIC8gNSUpIDFweCAxMHB4IDEwcHggMXB4XCIsXHJcbiAgICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICAgIHRyYW5zZm9ybTogXCJzY2FsZSgwLjk5KVwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH19XHJcbiAgICA+XHJcbiAgICAgIDxpZnJhbWVcclxuICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgYm9yZGVyUmFkaXVzOiAxMCxcclxuICAgICAgICAgIGJvcmRlcjogXCJ3aGl0ZSBzb2xpZCAwXCIsXHJcbiAgICAgICAgICBtYXJnaW46IDEwLFxyXG4gICAgICAgIH19XHJcbiAgICAgICAgc3JjPXt2aWRlb31cclxuICAgICAgICBoZWlnaHQ9ezE4MH1cclxuICAgICAgICB3aWR0aD17MzEwfVxyXG4gICAgICAvPlxyXG5cclxuICAgICAgPEZsZXhcclxuICAgICAgICBteD17MTV9XHJcbiAgICAgICAgb25DbGljaz17b25DbGlja31cclxuICAgICAgICBzeD17eyBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLCBjdXJzb3I6IFwicG9pbnRlclwiIH19XHJcbiAgICAgID5cclxuICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgY29sb3I9XCJiaWdTdG9uZVwiXHJcbiAgICAgICAgICBtYj1cIjVweFwiXHJcbiAgICAgICAgICBzeD17eyBmb250U2l6ZTogMTUsIGZvbnRXZWlnaHQ6IFwiaGVhZGluZ1wiIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAge25hbWV9XHJcbiAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgIDxUZXh0IGNvbG9yPVwib3V0ZXJTcGFjZVwiIHN4PXt7IGZvbnRTaXplOiAwLCBvcGFjaXR5OiAwLjYgfX0+XHJcbiAgICAgICAgICB7ZGVzY3JpcHRpb259XHJcbiAgICAgICAgPC9UZXh0PlxyXG4gICAgICA8L0ZsZXg+XHJcbiAgICA8L0xpbms+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IENhcmRWaWRlb1N5bXB0b21zO1xyXG4iLCJpbXBvcnQgUmVhY3QsIHsgRkMgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgQnV0dG9uLCBGbGV4LCBUZXh0LCBCb3gsIExpbmsgfSBmcm9tIFwidGhlbWUtdWlcIjtcclxuaW1wb3J0IEVhcnRoSWNvbiBmcm9tIFwiLi4vcHVibGljL2VhcnRoLnN2Z1wiO1xyXG5cclxuaW50ZXJmYWNlIEZvb3RlclByb3BzIHtcclxuICBvbkNsaWNrPzogKCkgPT4gdm9pZDtcclxufVxyXG5jb25zdCBGb290ZXI6IEZDPEZvb3RlclByb3BzPiA9ICh7IG9uQ2xpY2sgfSkgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICA8RmxleFxyXG4gICAgICBiZz1cIndoaXRlXCJcclxuICAgICAgcHg9ezE1fVxyXG4gICAgICBzeD17e1xyXG4gICAgICAgIGhlaWdodDogNzAsXHJcbiAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwic3BhY2UtYmV0d2VlblwiLFxyXG4gICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgZm9udEZhbWlseTogXCJIZWx2ZXRpY2EgTm93IFRleHRcIixcclxuICAgICAgfX1cclxuICAgID5cclxuICAgICAgPEJveCBtcj17MTV9PlxyXG4gICAgICAgIDxFYXJ0aEljb24gLz5cclxuICAgICAgPC9Cb3g+XHJcblxyXG4gICAgICA8RmxleFxyXG4gICAgICAgIG14PXsxMH1cclxuICAgICAgICBzeD17e1xyXG4gICAgICAgICAgd2lkdGg6IDE0MCxcclxuICAgICAgICAgIGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIsXHJcbiAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJzcGFjZS1iZXR3ZWVuXCIsXHJcbiAgICAgICAgfX1cclxuICAgICAgPlxyXG4gICAgICAgIDxUZXh0XHJcbiAgICAgICAgICBjb2xvcj1cIm91dGVyU3BhY2VcIlxyXG4gICAgICAgICAgbWI9XCI1cHhcIlxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgZm9udFNpemU6IDIsXHJcbiAgICAgICAgICAgIGZvbnRXZWlnaHQ6IFwiaGVhZGluZ1wiLFxyXG4gICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBMaXZlIE1hcCBpbmZvXHJcbiAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgIDxUZXh0XHJcbiAgICAgICAgICBjb2xvcj1cIm91dGVyU3BhY2VcIlxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgZm9udFNpemU6IDAsXHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNixcclxuICAgICAgICAgICAgbGluZUhlaWdodDogXCIxNnB4XCIsXHJcbiAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIFdhdGNoIHRoZSBsaXZlIHVwZGF0ZSBieSBlYWNoIGNvdW50cmllc1xyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgPC9GbGV4PlxyXG4gICAgICA8TGluayBocmVmPVwiaHR0cHM6Ly9iYW5kby50cGhjbS5nb3Yudm4vb2dpc1wiIHRhcmdldD1cIl9ibGFua1wiPlxyXG4gICAgICAgIDxCdXR0b24gb25DbGljaz17b25DbGlja30gdmFyaWFudD1cInRlcnRpYXJ5XCI+XHJcbiAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgIGZvbnRTaXplOiAxLFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBXYXRjaCBMaXZlXHJcbiAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgIDwvTGluaz5cclxuICAgIDwvRmxleD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRm9vdGVyO1xyXG4iLCJpbXBvcnQgUmVhY3QsIHsgRkMsIHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgRmxleCwgVGV4dCB9IGZyb20gXCJ0aGVtZS11aVwiO1xyXG5cclxuaW50ZXJmYWNlIEl0ZW1Qcm9wcyB7XHJcbiAgaWQ6IHN0cmluZyB8IG51bWJlcjtcclxuICBsYWJlbDogc3RyaW5nO1xyXG4gIHZhbHVlOiBzdHJpbmc7XHJcbn1cclxuXHJcbmludGVyZmFjZSBTZWxlY3Rpb25Qcm9wcyB7XHJcbiAgaXRlbXM6IEl0ZW1Qcm9wc1tdO1xyXG4gIG9uQ2xpY2s/OiAodmFsdWU6IHN0cmluZykgPT4gdm9pZDtcclxuICBzZWxlY3RlZEl0ZW0/OiBJdGVtUHJvcHM7XHJcbn1cclxuXHJcbmNvbnN0IFNlbGVjdGlvbjogRkM8U2VsZWN0aW9uUHJvcHM+ID0gKHsgaXRlbXMsIG9uQ2xpY2ssIHNlbGVjdGVkSXRlbSB9KSA9PiB7XHJcbiAgY29uc3QgW3NlbGVjdGVkLCBzZXRTZWxlY3RlZF0gPSB1c2VTdGF0ZTxJdGVtUHJvcHM+KHNlbGVjdGVkSXRlbSA/PyBpdGVtc1swXSk7XHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChvbkNsaWNrKSBvbkNsaWNrKHNlbGVjdGVkLnZhbHVlKTtcclxuICB9LCBbc2VsZWN0ZWRdKTtcclxuICByZXR1cm4gKFxyXG4gICAgPEZsZXhcclxuICAgICAgYmc9XCJ3aGl0ZVwiXHJcbiAgICAgIG14PXsxNX1cclxuICAgICAgbWI9ezEwfVxyXG4gICAgICBwPVwiNXB4XCJcclxuICAgICAgc3g9e3tcclxuICAgICAgICBoZWlnaHQ6IDQ3LFxyXG4gICAgICAgIGJvcmRlclJhZGl1czogMjUsXHJcbiAgICAgICAgYm94U2hhZG93OiBcInJnYigwIDAgMCAvIDUlKSAxcHggOHB4IDhweCAxcHhcIixcclxuICAgICAgfX1cclxuICAgID5cclxuICAgICAge2l0ZW1zLm1hcCgoaXRlbSkgPT4gKFxyXG4gICAgICAgIDxGbGV4XHJcbiAgICAgICAgICBrZXk9e2l0ZW0uaWR9XHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XHJcbiAgICAgICAgICAgIHNldFNlbGVjdGVkKGl0ZW0pO1xyXG4gICAgICAgICAgfX1cclxuICAgICAgICAgIGJnPXtzZWxlY3RlZCA9PT0gaXRlbSA/IFwiY2hlbHNlYUN1Y3VtYmVyMThcIiA6IFwid2hpdGVcIn1cclxuICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgIGJvcmRlclJhZGl1czogMjUsXHJcbiAgICAgICAgICAgIGZsZXg6IDEsXHJcbiAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxyXG4gICAgICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxyXG4gICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgIGZvbnRXZWlnaHQ6IHNlbGVjdGVkID09PSBpdGVtID8gXCI3MDBcIiA6IFwiYm9keVwiLFxyXG4gICAgICAgICAgICAgIGZvbnRTaXplOiAxNSxcclxuICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiAyMCxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgY29sb3I9e3NlbGVjdGVkID09PSBpdGVtID8gXCJjaGVsc2VhQ3VjdW1iZXJcIiA6IFwibWlzdEdyYXlcIn1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAge2l0ZW0ubGFiZWx9XHJcbiAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgPC9GbGV4PlxyXG4gICAgICApKX1cclxuICAgIDwvRmxleD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgU2VsZWN0aW9uO1xyXG4iLCJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IFJlYWN0LCB7IEZDLCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGbGV4LCBJbWFnZSwgVGV4dCwgQnV0dG9uLCBHcmlkIH0gZnJvbSBcInRoZW1lLXVpXCI7XHJcbmltcG9ydCBDYXJkQnV0dG9uIGZyb20gXCIuLi9jb21wb25lbnRzL0NhcmRCdXR0b25cIjtcclxuaW1wb3J0IENhcmRQcmV2ZW50aW9uIGZyb20gXCIuLi9jb21wb25lbnRzL0NhcmRQcmV2ZW50aW9uXCI7XHJcbmltcG9ydCBDYXJkVmlkZW9TeW1wdG9tcyBmcm9tIFwiLi4vY29tcG9uZW50cy9DYXJkVmlkZW9TeW1wdG9tc1wiO1xyXG5pbXBvcnQgQ2FyZEltYWdlU3ltcHRvbXMgZnJvbSBcIi4uL2NvbXBvbmVudHMvQ2FyZEltYWdlU3ltcHRvbXNcIjtcclxuaW1wb3J0IENhcmREaWFnbm9zaXMgZnJvbSBcIi4uL2NvbXBvbmVudHMvQ2FyZERpYWdub3Npc1wiO1xyXG5pbXBvcnQgRm9vdGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0Zvb3RlclwiO1xyXG5pbXBvcnQgTWVudUljb24gZnJvbSBcIi4uL3B1YmxpYy9tZW51LnN2Z1wiO1xyXG5pbXBvcnQgR2xhc3NJY29uIGZyb20gXCIuLi9wdWJsaWMvZ2xhc3Muc3ZnXCI7XHJcbmltcG9ydCBTZWxlY3Rpb24gZnJvbSBcIi4uL2NvbXBvbmVudHMvU2VsZWN0aW9uXCI7XHJcbmltcG9ydCBQaG9uZUljb24gZnJvbSBcIi4uL3B1YmxpYy9waG9uZS5zdmdcIjtcclxuXHJcbmNvbnN0IEl0ZW1zID0gW1xyXG4gIHtcclxuICAgIGlkOiAxLFxyXG4gICAgbGFiZWw6IFwiUHJldmVudGlvblwiLFxyXG4gICAgdmFsdWU6IFwiMVwiLFxyXG4gIH0sXHJcbiAge1xyXG4gICAgaWQ6IDIsXHJcbiAgICBsYWJlbDogXCJTeW1wdG9tc1wiLFxyXG4gICAgdmFsdWU6IFwiMlwiLFxyXG4gIH0sXHJcbiAge1xyXG4gICAgaWQ6IDMsXHJcbiAgICBsYWJlbDogXCJEaWFnbm9zaXNcIixcclxuICAgIHZhbHVlOiBcIjNcIixcclxuICB9LFxyXG5dO1xyXG5cclxuY29uc3QgUHJlSXRlbXMgPSBbXHJcbiAge1xyXG4gICAgaW1hZ2U6IFwiL2ltYWdlcy9sb2dvMS5QTkdcIixcclxuICAgIG5hbWVDYXJkOiBcIldlYXIgYSBmYWNlbWFza1wiLFxyXG4gICAgZGVzY3JpcHRpb246IFwiWW91IHNob3VsZCB3ZWFyIGZhY2VtYXNrIHdoZW4geW91IGFyZSBhcm91bmQgb3RoZXIgcGVvcGxlXCIsXHJcbiAgICBudW1iZXJEaXNjdXNzaW9uczogXCIxNTNcIixcclxuICB9LFxyXG4gIHtcclxuICAgIGltYWdlOiBcIi9pbWFnZXMvbG9nbzIuUE5HXCIsXHJcbiAgICBuYW1lQ2FyZDogXCJBdm9pZCBjbG9zZSBjb250YWN0XCIsXHJcbiAgICBkZXNjcmlwdGlvbjogXCJQdXQgZGlzdGFuY2UgYmV0d2VlbiB5b3Vyc2VsZiBhbmQgb3RoZXIgcGVvcGxlXCIsXHJcbiAgICBudW1iZXJEaXNjdXNzaW9uczogXCIxMjdcIixcclxuICB9LFxyXG4gIHtcclxuICAgIGltYWdlOiBcIi9pbWFnZXMvbG9nbzMuUE5HXCIsXHJcbiAgICBuYW1lQ2FyZDogXCJTdGF5IGhvbWUgaWYgeW914oCZcmUgc2lja1wiLFxyXG4gICAgZGVzY3JpcHRpb246IFwiU3RheSBob21lIGlmIHlvdSBhcmUgc2ljaywgZXhjZXB0IHRvIGdldCBtZWRpY2FsIGNhcmUuXCIsXHJcbiAgICBudW1iZXJEaXNjdXNzaW9uczogXCI3OFwiLFxyXG4gIH0sXHJcbiAge1xyXG4gICAgaW1hZ2U6IFwiL2ltYWdlcy9sb2dvNC5QTkdcIixcclxuICAgIG5hbWVDYXJkOiBcIkNsZWFuIHlvdXIgaGFuZHMgb2Z0ZW5cIixcclxuICAgIGRlc2NyaXB0aW9uOiBcIkNsZWFuIHlvdXIgaGFuZHMgb2Z0ZW5cIixcclxuICAgIG51bWJlckRpc2N1c3Npb25zOiBcIjMyNFwiLFxyXG4gIH0sXHJcbl07XHJcblxyXG5jb25zdCBkYXNoYm9hcmQ6IEZDID0gKCkgPT4ge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGNvbnN0IFtjbG9zZSwgc2V0Q2xvc2VdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFt2YWwsIHNldFZhbF0gPSB1c2VTdGF0ZShcIjFcIik7XHJcblxyXG4gIGNvbnN0IGhhbmRsZUNsaWNrID0gKCkgPT4ge1xyXG4gICAgaWYgKCFjbG9zZSkgc2V0Q2xvc2UodHJ1ZSk7XHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxGbGV4XHJcbiAgICAgIHN4PXt7XHJcbiAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxyXG4gICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxyXG4gICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgIH19XHJcbiAgICA+XHJcbiAgICAgIDxGbGV4XHJcbiAgICAgICAgYmc9XCIjRjRGMEVCXCJcclxuICAgICAgICBzeD17e1xyXG4gICAgICAgICAgd2lkdGg6IDM2MCxcclxuICAgICAgICAgIGhlaWdodDogNzAwLFxyXG4gICAgICAgICAgZmxleERpcmVjdGlvbjogXCJjb2x1bW5cIixcclxuICAgICAgICAgIG92ZXJmbG93WTogXCJhdXRvXCIsXHJcbiAgICAgICAgfX1cclxuICAgICAgPlxyXG4gICAgICAgIDxGbGV4XHJcbiAgICAgICAgICBiZz1cIiNFRDNEM0RcIlxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgaGVpZ2h0OiBjbG9zZSA/IDkwIDogMjAwLFxyXG4gICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxGbGV4XHJcbiAgICAgICAgICAgIGJnPVwiI0VEM0QzRFwiXHJcbiAgICAgICAgICAgIG10PXsyMH1cclxuICAgICAgICAgICAgbXg9ezE1fVxyXG4gICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDMwLFxyXG4gICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcInNwYWNlLWJldHdlZW5cIixcclxuICAgICAgICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgICAgICAgIHpJbmRleDogOTksXHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxGbGV4XHJcbiAgICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBcImFsbCAwLjNzIGVhc2UtaW4tb3V0IDBzXCIsXHJcbiAgICAgICAgICAgICAgICBcIjphY3RpdmVcIjoge1xyXG4gICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45MClcIixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxNZW51SWNvbiAvPlxyXG4gICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgIDxGbGV4XHJcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcm91dGVyLnJlbG9hZCgpO1xyXG4gICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBcImFsbCAwLjNzIGVhc2UtaW4tb3V0IDBzXCIsXHJcbiAgICAgICAgICAgICAgICBcIjphY3RpdmVcIjoge1xyXG4gICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45NSlcIixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxJbWFnZSBzcmM9XCIvaW1hZ2VzL2Nvcm9uYS5QTkdcIiB2YXJpYW50PVwiY29yb25hXCIgLz5cclxuICAgICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgICAgICA8RmxleFxyXG4gICAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogXCJhbGwgMC4zcyBlYXNlLWluLW91dCAwc1wiLFxyXG4gICAgICAgICAgICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiBcInNjYWxlKDAuOTUpXCIsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8R2xhc3NJY29uIC8+XHJcbiAgICAgICAgICAgIDwvRmxleD5cclxuICAgICAgICAgIDwvRmxleD5cclxuICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgeyFjbG9zZSAmJiAoXHJcbiAgICAgICAgICA8RmxleFxyXG4gICAgICAgICAgICBtdD17LTEzMH1cclxuICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICB6SW5kZXg6IDk5LFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8Q2FyZEJ1dHRvblxyXG4gICAgICAgICAgICAgIHRpdGxlPVwiQ29yb25hdmlydXMgZGlzZWFzZSAoQ09WSUQtMTkpIGFkdmljZSBmb3IgdGhlIHB1YmxpY1wiXHJcbiAgICAgICAgICAgICAgY29udGVudD1cIlN0YXkgYXdhcmUgb2YgdGhlIGxhdGVzdCBpbmZvcm1hdGlvbiBvbiB0aGUgQ09WSUQtMTkgb3V0YnJlYWtcIlxyXG4gICAgICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZUNsaWNrfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgICl9XHJcbiAgICAgICAgPEZsZXhcclxuICAgICAgICAgIG10PXstMTYwfVxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgaGVpZ2h0OiA0ODAsXHJcbiAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPEZsZXhcclxuICAgICAgICAgICAgbXQ9e2Nsb3NlID8gMTM3IDogMTg1fVxyXG4gICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8RmxleFxyXG4gICAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8U2VsZWN0aW9uXHJcbiAgICAgICAgICAgICAgICBpdGVtcz17SXRlbXN9XHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsodmFsdWUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgc2V0VmFsKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgaWYgKCFjbG9zZSAmJiB2YWx1ZSAhPT0gXCIxXCIpIHNldENsb3NlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkSXRlbT17SXRlbXNbMF19XHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICB7dmFsID09PSBcIjFcIiA/IChcclxuICAgICAgICAgICAgICAgIDxGbGV4XHJcbiAgICAgICAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleERpcmVjdGlvbjogXCJjb2x1bW5cIixcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIHtQcmVJdGVtcy5tYXAoKGl0ZW0sIGluZGV4KSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEZsZXgga2V5PXtpbmRleH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Q2FyZFByZXZlbnRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17aGFuZGxlQ2xpY2t9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGltYWdlPXtpdGVtLmltYWdlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lQ2FyZD17aXRlbS5uYW1lQ2FyZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb249e2l0ZW0uZGVzY3JpcHRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlckRpc2N1c3Npb25zPXtpdGVtLm51bWJlckRpc2N1c3Npb25zfVxyXG4gICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgICAgICkpfVxyXG4gICAgICAgICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgICAgICAgICkgOiB2YWwgPT09IFwiMlwiID8gKFxyXG4gICAgICAgICAgICAgICAgPEZsZXhcclxuICAgICAgICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPEZsZXg+XHJcbiAgICAgICAgICAgICAgICAgICAgPENhcmRWaWRlb1N5bXB0b21zXHJcbiAgICAgICAgICAgICAgICAgICAgICBsaW5rPVwiaHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1XUEY3S2EzdE5TVVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICB2aWRlbz1cImh0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2VtYmVkL1dQRjdLYTN0TlNVXCJcclxuICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJXaGF0IENvcm9uYXZpcnVzIFN5bXB0b21zIExvb2sgTGlrZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbj1cIkFmdGVyIGJlaW5nIGV4cG9zZWQgdG8gdGhlIHZpcnVzIHRoYXQgY2F1c2UgQ09WSUQtMTksIGl0IGNhbiB0YWtlIGFzIDIgdG8gNCBkYXlzIHRvIGRldmVsb3AuXCJcclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgICAgIDxHcmlkIGNvbHVtbnM9ezJ9IHB4PXsxNX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZsZXg+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Q2FyZEltYWdlU3ltcHRvbXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U9XCIvaW1hZ2VzL2ZlcnZlci5QTkdcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwiRmV2ZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbj1cIkhlIHNldmVyaXR5IG9mIENPVklELTE5IHN5bXB0b21zIGNhbiByYW5nZSBmcm9tIHZlcnkgbWlsZCB0byBzZXZlcmVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZsZXg+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Q2FyZEltYWdlU3ltcHRvbXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U9XCIvaW1hZ2VzL2NvdWdoLlBOR1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJDb3VnaFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uPVwiU3VjaCBhcyBoZWFydCBvciBsdW5nIGRpc2Vhc2Ugb3IgZGlhYmV0ZXMsIG1heSBiZSBhdCBoaWdoZXIgcmlzayBvZiBzZXJpb3VzIGlsbG5lc3NcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgICAgIDwvR3JpZD5cclxuICAgICAgICAgICAgICAgIDwvRmxleD5cclxuICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgPEZsZXhcclxuICAgICAgICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIDxGbGV4IHN4PXt7IHdpZHRoOiAzNjAgfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPENhcmREaWFnbm9zaXNcclxuICAgICAgICAgICAgICAgICAgICAgIGltYWdlPVwiL2ltYWdlcy9EaWFnbm9zaXMuUE5HXCJcclxuICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPVwiQm9vayBUZXN0IEFwb29pdG1lbnRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgY29udGVudD1cIklmIHlvdSBkZXZlbG9wIHN5bXB0b21zIG9mIGNvcm9uYXZpcnVzIGRpc2Vhc2UgMjAxOSAoQy1PVklELTE5KSBhbmQgeW91J3ZlIGJlZW4gZXhwb3NlZCB0byB0aGUgdmlydXMsIGNvbnRhY3QgeW91ciBkb2N0b3IuXCJcclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgICAgIDxUZXh0XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJiaWdTdG9uZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgbXQ9ezMwfVxyXG4gICAgICAgICAgICAgICAgICAgIG1iPVwiNXB4XCJcclxuICAgICAgICAgICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgZm9udFNpemU6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgZm9udFdlaWdodDogXCJoZWFkaW5nXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIEdpdmUgYSBNaXNzIENhbGwgT25cclxuICAgICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwaG9uZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4XHJcbiAgICAgICAgICAgICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPFBob25lSWNvbiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPFRleHQgbWw9ezEwfT4wMjgtMzkzMC05OTEyPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvRmxleD5cclxuICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L0ZsZXg+XHJcbiAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgICAgICA8RmxleFxyXG4gICAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogY2xvc2UgPyBcImZpeGVkXCIgOiBcInJlbGF0aXZlXCIsXHJcbiAgICAgICAgICAgICAgICB0b3A6IGNsb3NlID8gNjMwIDogMCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBjbG9zZSA/IDM2MCA6IDM0MyxcclxuICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPEZvb3RlciBvbkNsaWNrPXtoYW5kbGVDbGlja30gLz5cclxuICAgICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgICAgPC9GbGV4PlxyXG4gICAgICAgIDwvRmxleD5cclxuICAgICAgPC9GbGV4PlxyXG4gICAgPC9GbGV4PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBkYXNoYm9hcmQ7XHJcbiIsInZhciBfcGF0aDtcblxuZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmZ1bmN0aW9uIFN2Z0Fycm93KHByb3BzKSB7XG4gIHJldHVybiAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLCBfZXh0ZW5kcyh7XG4gICAgeG1sbnM6IFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIixcbiAgICB3aWR0aDogMTYuODEzLFxuICAgIGhlaWdodDogMTAuODY5XG4gIH0sIHByb3BzKSwgX3BhdGggfHwgKF9wYXRoID0gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIlBhdGggNDMzNlwiLFxuICAgIGQ6IFwiTS44NzcgNi4zMTFoMTIuOTM2bC0zLjA3NSAzLjA2YS44NzcuODc3IDAgMDAxLjIzNyAxLjI0M2w0LjU4LTQuNTU4YS44NzcuODc3IDAgMDAwLTEuMjQxTDExLjk3NS4yNTdhLjg3Ny44NzcgMCAwMC0xLjIzNyAxLjI0MmwzLjA3NSAzLjA1OUguODc3YS44NzYuODc2IDAgMTAwIDEuNzUzelwiLFxuICAgIGZpbGw6IFwiIzdkYTc1MVwiXG4gIH0pKSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IFN2Z0Fycm93OyIsInZhciBfcGF0aCwgX3BhdGgyLCBfcGF0aDMsIF9wYXRoNCwgX3BhdGg1LCBfcGF0aDYsIF9wYXRoNztcblxuZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmZ1bmN0aW9uIFN2Z0VhcnRoKHByb3BzKSB7XG4gIHJldHVybiAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLCBfZXh0ZW5kcyh7XG4gICAgeG1sbnM6IFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIixcbiAgICB3aWR0aDogNDMuNDI0LFxuICAgIGhlaWdodDogNDcuMTA0XG4gIH0sIHByb3BzKSwgX3BhdGggfHwgKF9wYXRoID0gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIlBhdGggMjg1NjZcIixcbiAgICBkOiBcIk0zNy4xNDQgMzUuNTEzYS43MzYuNzM2IDAgMDAtLjk5NS4zMDdxLS4wNjUuMTIzLS4xMzIuMjQ0aC0yLjk0OWEyNC40MjEgMjQuNDIxIDAgMDAuODI5LTIuNzc1LjczNi43MzYgMCAxMC0xLjQzMy0uMzM4IDIyLjMzMyAyMi4zMzMgMCAwMS0uOTc4IDMuMTEzaC01LjY1M2E1Ny4zNzkgNTcuMzc5IDAgMDAuNjU4LTguMWgzLjY4NWEuNzM2LjczNiAwIDEwMC0xLjQ3MmgtMy42ODVhNTYuNDcyIDU2LjQ3MiAwIDAwLS43OC04LjgzMmg1LjQ3NWEyMi4wNCAyMi4wNCAwIDAxLjg5MyAyLjQyMS43MzYuNzM2IDAgMTAxLjQwOC0uNDI5cS0uMzE0LTEuMDMxLS43LTEuOTkyaDIuODA2Yy4wNzQuMTIyLjE0OS4yNDQuMjIxLjM2OGEuNzM2LjczNiAwIDEwMS4yNzQtLjczN2wtLjMtLjQ5M2EuNzM1LjczNSAwIDAwLS4zMDYtLjQ3OSAxOS45IDE5LjkgMCAwMC0yMS4yLTguNDMxLjczNi43MzYgMCAwMC4zMzggMS40MzNxLjcxNi0uMTY5IDEuNDQ4LS4yNzlhMTguOSAxOC45IDAgMDAtMi43NiA3LjE0N2gtNi4wNGEuNzMyLjczMiAwIDAwLS4zNDIgMEg1LjE1MnEuMjc0LS4zNjUuNTY2LS43MThhLjczNi43MzYgMCAwMC0xLjEzMi0uOTQxIDE5LjcyNSAxOS43MjUgMCAwMC0xLjMyMyAxLjc4OS43MzUuNzM1IDAgMDAtLjMwOC40ODUgMTkuODczIDE5Ljg3MyAwIDEwMzQuNSAxOS43LjczNi43MzYgMCAwMC0uMzExLS45OTF6bS02LjMwOCAyLjAyM2ExNy4zNjkgMTcuMzY5IDAgMDEtMy4xOCA0LjU3MyAxMS44ODcgMTEuODg3IDAgMDEtNC44IDMuMDUxIDIwLjk0NSAyMC45NDUgMCAwMDIuNzE2LTcuNjI1em0tNS40MDgtMjEuMzQ0YTE5Ljc1NCAxOS43NTQgMCAwMC0yLjU2Ny02Ljg4NyAxMy43NzkgMTMuNzc5IDAgMDE3LjYwOCA2Ljg4N3ptMy43NzItNC44MjNhMTguNTEyIDE4LjUxMiAwIDAxNS4zOSA0LjgyM2gtMi40NjZBMTguMjQzIDE4LjI0MyAwIDAwMjguMiAxMC44MnEuNTA3LjI1OSAxIC41NDl6bS0xMi40IDEuNDA3Yy45MTItMi40NyAyLjA2Mi0zLjk0NCAzLjA3NS0zLjk0NCAxLjE4NyAwIDIuNDggMS45IDMuNDYgNS4wODUuMjE5LjcxMy40MTggMS40NzQuNiAyLjI3NWgtOC4xMTlhMjYuNjMyIDI2LjYzMiAwIDAxLjk4NC0zLjQxNnptLTIuNzQ4IDExLjUxYS43MzUuNzM1IDAgMDAuNzc5LS42OTEgNTAuNSA1MC41IDAgMDEuNjg5LTUuOTM1aDguNzA2YTUzLjY0IDUzLjY0IDAgMDEuNzkzIDguODMyaC0zLjY3NWEuNzM2LjczNiAwIDEwMCAxLjQ3MmgzLjY3NWE1NC44IDU0LjggMCAwMS0uNjY2IDguMWgtMy4wMDlhLjczNi43MzYgMCAxMDAgMS40NzJoMi43NDFhMzEuMzYgMzEuMzYgMCAwMS0uNzUzIDMuMDExYy0uOTggMy4xODQtMi4yNzMgNS4wODUtMy40NiA1LjA4NS0uOTkzIDAtMi4wODQtMS4zNTgtMi45OTMtMy43MjVhLjczNi43MzYgMCAxMC0xLjM3NC41MjcgMTIuNyAxMi43IDAgMDAxLjM4OCAyLjcyOWMtMy4zMDctMS4wNTktNi4xNDQtMy44NDMtNy45OTMtNy42M2EuNzM2LjczNiAwIDAwLS4wNjgtMS40NjloLS41NzlhMjMuOTIxIDIzLjkyMSAwIDAxLTEuNjE3LTguMUg4LjFhLjczNi43MzYgMCAxMDAtMS40NzJINi42MzVhMjMuNjgzIDIzLjY4MyAwIDAxMS45MjctOC44MzJoNS40NjZhNTIuNDY0IDUyLjQ2NCAwIDAwLS42NyA1Ljg0NC43MzYuNzM2IDAgMDAuNjkxLjc4MnpNMS40OTEgMjcuOTY4aDMuNjc1YTI1LjUzMiAyNS41MzIgMCAwMDEuNTExIDguMUgzLjczNGExOC4yNzQgMTguMjc0IDAgMDEtMi4yNDYtOC4xem01LjQ3Ni0xMC4zYTI1LjM3IDI1LjM3IDAgMDAtMS44IDguODMyaC0zLjY4YTE4LjQ0MiAxOC40NDIgMCAwMTIuNjY5LTguODR6TTQuNjM4IDM3LjU0aDIuNjM5YTIwLjMyOSAyMC4zMjkgMCAwMDIuMTE1IDMuNjYxIDE3LjAyOSAxNy4wMjkgMCAwMDIuMTU5IDIuNDQxIDE4LjUzMSAxOC41MzEgMCAwMS02LjkxNi02LjEwNnptMjMuNTI1IDYuMTJxLjI3My0uMjUyLjUzOS0uNTJhMTguOTczIDE4Ljk3MyAwIDAwMy43NjgtNS42MDhoMi42NDhhMTguMzcxIDE4LjM3MSAwIDAxLTYuOTU1IDYuMTI4elwiLFxuICAgIGZpbGw6IFwiI2M4YWY5MVwiXG4gIH0pKSwgX3BhdGgyIHx8IChfcGF0aDIgPSAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiUGF0aCAyODU2N1wiLFxuICAgIGQ6IFwiTTcuNjY4IDE0LjU4M2EuNzM2LjczNiAwIDAwLjg2My0uMDA1Yy4yMjMtLjE2MyA1LjQ1My00LjA0NiA1LjQ1My04LjY5YTUuODg4IDUuODg4IDAgMDAtMTEuNzc2IDBjMCA0Ljc0IDUuMjM3IDguNTM2IDUuNDYgOC42OTV6bS40MjgtMTMuMTExYTQuNDIxIDQuNDIxIDAgMDE0LjQxMiA0LjQxNmMwIDMuMi0zLjIzMyA2LjE3NC00LjQyMSA3LjE2LTEuMTg4LS45NjktNC40MTEtMy45LTQuNDExLTcuMTZhNC40MjEgNC40MjEgMCAwMTQuNDItNC40MTZ6XCIsXG4gICAgZmlsbDogXCIjZWQzZDNkXCJcbiAgfSkpLCBfcGF0aDMgfHwgKF9wYXRoMyA9IC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLCB7XG4gICAgXCJkYXRhLW5hbWVcIjogXCJQYXRoIDI4NTY4XCIsXG4gICAgZDogXCJNMTEuMDQgNS44ODhhLjczNi43MzYgMCAwMC0xLjQ3MiAwIDEuNDcyIDEuNDcyIDAgMTEtMS40NzItMS40NzIuNzM2LjczNiAwIDEwMC0xLjQ3MiAyLjk0NCAyLjk0NCAwIDEwMi45NDQgMi45NDR6XCIsXG4gICAgZmlsbDogXCIjZWQzZDNkXCJcbiAgfSkpLCBfcGF0aDQgfHwgKF9wYXRoNCA9IC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLCB7XG4gICAgXCJkYXRhLW5hbWVcIjogXCJQYXRoIDI4NTY5XCIsXG4gICAgZDogXCJNMzcuNTM2IDE5Ljg3MmE1LjkgNS45IDAgMDAtNS44ODggNS44ODhjMCA0Ljc0IDUuMjM3IDguNTM2IDUuNDYgOC42OTVhLjczNi43MzYgMCAwMC44NjMtLjAwNWMuMjIzLS4xNjMgNS40NTMtNC4wNDYgNS40NTMtOC42OWE1LjkgNS45IDAgMDAtNS44ODgtNS44ODh6bTAgMTMuMDQ4Yy0xLjE4OC0uOTY5LTQuNDExLTMuOS00LjQxMS03LjE2YTQuNDE2IDQuNDE2IDAgMTE4LjgzMiAwYy0uMDA5IDMuMTk2LTMuMjM4IDYuMTc0LTQuNDI2IDcuMTZ6XCIsXG4gICAgZmlsbDogXCIjN2RhNzUxXCJcbiAgfSkpLCBfcGF0aDUgfHwgKF9wYXRoNSA9IC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLCB7XG4gICAgXCJkYXRhLW5hbWVcIjogXCJQYXRoIDI4NTcwXCIsXG4gICAgZDogXCJNMzkuNzQ0IDI1LjAyNGEuNzM2LjczNiAwIDAwLS43MzYuNzM2IDEuNDcyIDEuNDcyIDAgMTEtMS40NzItMS40NzIuNzM2LjczNiAwIDEwMC0xLjQ3MiAyLjk0NCAyLjk0NCAwIDEwMi45NDQgMi45NDQuNzM2LjczNiAwIDAwLS43MzYtLjczNnpcIixcbiAgICBmaWxsOiBcIiM3ZGE3NTFcIlxuICB9KSksIF9wYXRoNiB8fCAoX3BhdGg2ID0gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIlBhdGggMjg1NzFcIixcbiAgICBkOiBcIk04LjgzMiAzMS42NDhjMCA0Ljc0IDUuMjM3IDguNTM2IDUuNDYgOC42OTVhLjczNi43MzYgMCAwMC44NjMtLjAwNWMuMjIzLS4xNjMgNS40NTMtNC4wNDYgNS40NTMtOC42OWE1Ljg4OCA1Ljg4OCAwIDEwLTExLjc3NiAwem01Ljg4OC00LjQxNmE0LjQyMSA0LjQyMSAwIDAxNC40MTYgNC40MTZjMCAzLjItMy4yMzMgNi4xNzQtNC40MjEgNy4xNi0xLjE4OC0uOTY5LTQuNDExLTMuOS00LjQxMS03LjE2YTQuNDIxIDQuNDIxIDAgMDE0LjQxNi00LjQxNnpcIixcbiAgICBmaWxsOiBcIiNjOGFmOTFcIlxuICB9KSksIF9wYXRoNyB8fCAoX3BhdGg3ID0gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIlBhdGggMjg1NzJcIixcbiAgICBkOiBcIk0xNC43MiAzNC41OTJhMi45NDcgMi45NDcgMCAwMDIuOTQ0LTIuOTQ0LjczNi43MzYgMCAwMC0xLjQ3MiAwIDEuNDcyIDEuNDcyIDAgMTEtMS40NzItMS40NzIuNzM2LjczNiAwIDEwMC0xLjQ3MiAyLjk0NCAyLjk0NCAwIDEwMCA1Ljg4OHpcIixcbiAgICBmaWxsOiBcIiNjOGFmOTFcIlxuICB9KSkpO1xufVxuXG5leHBvcnQgZGVmYXVsdCBTdmdFYXJ0aDsiLCJ2YXIgX2csIF9nMjtcblxuZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmZ1bmN0aW9uIFN2Z0dsYXNzKHByb3BzKSB7XG4gIHJldHVybiAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLCBfZXh0ZW5kcyh7XG4gICAgeG1sbnM6IFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIixcbiAgICB3aWR0aDogMjIuOCxcbiAgICBoZWlnaHQ6IDIyLjhcbiAgfSwgcHJvcHMpLCBfZyB8fCAoX2cgPSAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcImdcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiR3JvdXAgMjUyOFwiXG4gIH0sIC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwiZ1wiLCB7XG4gICAgXCJkYXRhLW5hbWVcIjogXCJHcm91cCAyNTI3XCJcbiAgfSwgLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIlBhdGggMTU0M1wiLFxuICAgIGQ6IFwiTTEwLjA4OC40YTkuNjg4IDkuNjg4IDAgMTA5LjY4OCA5LjY4OEE5LjcgOS43IDAgMDAxMC4wODguNHptMCAxNy41ODhhNy45IDcuOSAwIDExNy45LTcuOSA3LjkwOSA3LjkwOSAwIDAxLTcuOSA3Ljl6XCIsXG4gICAgZmlsbDogXCIjZmZmXCIsXG4gICAgc3Ryb2tlOiBcIiNmZmZcIixcbiAgICBzdHJva2VXaWR0aDogMC44XG4gIH0pKSkpLCBfZzIgfHwgKF9nMiA9IC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwiZ1wiLCB7XG4gICAgXCJkYXRhLW5hbWVcIjogXCJHcm91cCAyNTMwXCJcbiAgfSwgLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJnXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIkdyb3VwIDI1MjlcIlxuICB9LCAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiUGF0aCAxNTQ0XCIsXG4gICAgZDogXCJNMjIuMTM4IDIwLjg3M2wtNS4xMjctNS4xMjdhLjg5NC44OTQgMCAxMC0xLjI2NSAxLjI2NWw1LjEyNyA1LjEyN2EuODk0Ljg5NCAwIDAwMS4yNjUtMS4yNjV6XCIsXG4gICAgZmlsbDogXCIjZmZmXCIsXG4gICAgc3Ryb2tlOiBcIiNmZmZcIixcbiAgICBzdHJva2VXaWR0aDogMC44XG4gIH0pKSkpKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgU3ZnR2xhc3M7IiwidmFyIF9nO1xuXG5mdW5jdGlvbiBfZXh0ZW5kcygpIHsgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9OyByZXR1cm4gX2V4dGVuZHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTsgfVxuXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tIFwicmVhY3RcIjtcblxuZnVuY3Rpb24gU3ZnTWVudShwcm9wcykge1xuICByZXR1cm4gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIiwgX2V4dGVuZHMoe1xuICAgIHhtbG5zOiBcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIsXG4gICAgd2lkdGg6IDI1LFxuICAgIGhlaWdodDogMTJcbiAgfSwgcHJvcHMpLCBfZyB8fCAoX2cgPSAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcImdcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiR3JvdXAgMjUyNVwiLFxuICAgIGZpbGw6IFwiI2ZmZlwiLFxuICAgIHRyYW5zZm9ybTogXCJ0cmFuc2xhdGUoLTE1IC0yNylcIlxuICB9LCAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInJlY3RcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiUmVjdGFuZ2xlIDMwNlwiLFxuICAgIHdpZHRoOiAyNSxcbiAgICBoZWlnaHQ6IDMsXG4gICAgcng6IDEuNSxcbiAgICB0cmFuc2Zvcm06IFwidHJhbnNsYXRlKDE1IDI3KVwiXG4gIH0pLCAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiUGF0aCA0MzQ3XCIsXG4gICAgZDogXCJNMTYuNSAzNmgxM2ExLjUgMS41IDAgMDEwIDNoLTEzYTEuNSAxLjUgMCAwMTAtM3pcIlxuICB9KSkpKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgU3ZnTWVudTsiLCJ2YXIgX3BhdGg7XG5cbmZ1bmN0aW9uIF9leHRlbmRzKCkgeyBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07IHJldHVybiBfZXh0ZW5kcy5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyB9XG5cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuXG5mdW5jdGlvbiBTdmdQaG9uZShwcm9wcykge1xuICByZXR1cm4gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIiwgX2V4dGVuZHMoe1xuICAgIHhtbG5zOiBcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIsXG4gICAgd2lkdGg6IDI0LFxuICAgIGhlaWdodDogMjQsXG4gICAgdmlld0JveDogXCIwIDAgMjcgMjdcIlxuICB9LCBwcm9wcyksIF9wYXRoIHx8IChfcGF0aCA9IC8qI19fUFVSRV9fKi9SZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLCB7XG4gICAgZDogXCJNMjMuMDQ0IDMuOTUyQTEzLjUgMTMuNSAwIDEwMjcgMTMuNWExMy41IDEzLjUgMCAwMC0zLjk1Ni05LjU0OHptLTIuNiAxNC44NTNsLS42ODQuNjc5YTMuNiAzLjYgMCAwMS0zLjQuOTcyIDExLjk2MiAxMS45NjIgMCAwMS0zLjQ3Mi0xLjU1NiAxNi4wNTEgMTYuMDUxIDAgMDEtMi43NDEtMi4yMUExNi4xNzQgMTYuMTc0IDAgMDE4LjEgMTQuMmExMi42IDEyLjYgMCAwMS0xLjUzOS0zLjExNCAzLjYgMy42IDAgMDEuOS0zLjY3N2wuOC0uOGEuNTcxLjU3MSAwIDAxLjgwOCAwTDExLjYgOS4xNDFhLjU3MS41NzEgMCAwMTAgLjgwOGwtMS40ODUgMS40ODVhMS4yMSAxLjIxIDAgMDAtLjEyNiAxLjU3NSAxNy4zNjcgMTcuMzY3IDAgMDAxLjc3OCAyLjA3NSAxNy4yNjEgMTcuMjYxIDAgMDAyLjM0IDEuOTY3IDEuMjE5IDEuMjE5IDAgMDAxLjU1Ny0uMTM1bDEuNDM2LTEuNDU3YS41NzEuNTcxIDAgMDEuODA4IDBMMjAuNDQzIDE4YS41NzEuNTcxIDAgMDEuMDAyLjgwNXpcIixcbiAgICBmaWxsOiBcIiNmZmZcIlxuICB9KSkpO1xufVxuXG5leHBvcnQgZGVmYXVsdCBTdmdQaG9uZTsiLCJ2YXIgX2c7XG5cbmZ1bmN0aW9uIF9leHRlbmRzKCkgeyBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07IHJldHVybiBfZXh0ZW5kcy5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyB9XG5cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuXG5mdW5jdGlvbiBTdmdXaGl0ZUFycm93KHByb3BzKSB7XG4gIHJldHVybiAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLCBfZXh0ZW5kcyh7XG4gICAgeG1sbnM6IFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIixcbiAgICB3aWR0aDogMTYuODEzLFxuICAgIGhlaWdodDogMTAuODY5XG4gIH0sIHByb3BzKSwgX2cgfHwgKF9nID0gLyojX19QVVJFX18qL1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJnXCIsIHtcbiAgICBcImRhdGEtbmFtZVwiOiBcIkdyb3VwIDM1NDlcIlxuICB9LCAvKiNfX1BVUkVfXyovUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIiwge1xuICAgIFwiZGF0YS1uYW1lXCI6IFwiUGF0aCA0MzM2XCIsXG4gICAgZDogXCJNLjg3NyA2LjMxMWgxMi45MzZsLTMuMDc1IDMuMDZhLjg3Ny44NzcgMCAwMDEuMjM3IDEuMjQzbDQuNTgtNC41NThhLjg3Ny44NzcgMCAwMDAtMS4yNDFMMTEuOTc1LjI1N2EuODc3Ljg3NyAwIDAwLTEuMjM3IDEuMjQybDMuMDc1IDMuMDU5SC44NzdhLjg3Ni44NzYgMCAxMDAgMS43NTN6XCIsXG4gICAgZmlsbDogXCIjZmZmXCJcbiAgfSkpKSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IFN2Z1doaXRlQXJyb3c7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9yb3V0ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInRoZW1lLXVpXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=