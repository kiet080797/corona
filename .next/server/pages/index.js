module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.tsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Boarding.tsx":
/*!*********************************!*\
  !*** ./components/Boarding.tsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\components\\Boarding.tsx";



const Boarding = ({
  object,
  onClick,
  onSkip
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    bg: "#ED3D3D",
    sx: {
      flexDirection: "column",
      justifyContent: "space-between"
    },
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mt: 50,
      sx: {
        height: "40%",
        alignItems: "center",
        justifyContent: "center"
      },
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
        src: object.image,
        variant: "boarding"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      sx: {
        flexDirection: "column"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "white",
        mx: 26,
        mb: 10,
        sx: {
          fontSize: 22,
          fontWeight: "heading"
        },
        children: object.title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "white",
        mx: 26,
        sx: {
          fontSize: 1,
          fontWeight: "body",
          lineHeight: "24px"
        },
        children: object.content
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 48,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      mx: 20,
      mb: 30,
      sx: {
        justifyContent: "space-between",
        alignItems: "center"
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
        color: "white",
        onClick: onSkip,
        sx: {
          fontSize: 15,
          fontWeight: "heading",
          cursor: "pointer",
          transition: "all 0.3s ease-in-out 0s",
          opacity: 0.8,
          ":hover": {
            opacity: 1
          },
          ":active": {
            transform: "scale(0.95)"
          }
        },
        children: "Skip"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
        ml: 35,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Box"], {
          bg: object.id === 1 ? "white" : "#870000",
          m: "2px",
          sx: {
            borderRadius: 999,
            width: 7,
            height: 7
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 88,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Box"], {
          bg: object.id === 2 ? "white" : "#870000",
          m: "2px",
          sx: {
            borderRadius: 999,
            width: 7,
            height: 7
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 97,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Box"], {
          m: "2px",
          bg: object.id === 3 ? "white" : "#870000",
          sx: {
            borderRadius: 999,
            width: 7,
            height: 7
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 106,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 87,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        onClick: onClick,
        variant: "primary",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Text"], {
          color: "chelseaCucumber",
          sx: {
            fontWeight: "heading",
            fontSize: 15
          },
          children: "Next"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 117,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 116,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Boarding);

/***/ }),

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! theme-ui */ "theme-ui");
/* harmony import */ var theme_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(theme_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Boarding__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Boarding */ "./components/Boarding.tsx");

var _jsxFileName = "C:\\Users\\Kiet Nguyen\\Desktop\\cv-web - Copy\\pages\\index.tsx";




const Objects = [{
  id: 1,
  image: "/images/boarding1.PNG",
  title: "Fever",
  content: "He severity of COVID-19 symptoms can range from very mild to severe. Some people have no symptoms. People who are older or have existing chronic  medical conditions."
}, {
  id: 2,
  image: "/images/boarding2.PNG",
  title: "Cough",
  content: "such as heart or lung disease or diabetes, may be at higher risk of serious illness. This is similar to what is seen with other respiratory illnesses, such as influenza."
}, {
  id: 3,
  image: "/images/boarding3.PNG",
  title: "Breathing Difficulty",
  content: "Contact your doctor or clinic right away if you have COVID-19 symptoms, you've been exposed to someone with COVID-19, or you live in or have traveled from an area with ongoing community spread of COVID-19"
}];

const index = () => {
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();
  const {
    0: next,
    1: setNext
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: change,
    1: setChange
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    const timer = setTimeout(() => {
      setChange(true);
    }, 2000);
    return () => clearTimeout(timer);
  }, []);

  const handleClick = () => {
    if (next === 2) {
      router.push("/dashboard");
    } else {
      setNext(next + 1);
    }
  };

  const handleSkip = () => {
    router.push("/dashboard");
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
    sx: {
      width: "100%",
      justifyContent: "center",
      alignItems: "center"
    },
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
      sx: {
        width: 360,
        height: 680
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Flex"], {
        sx: {
          position: "absolute",
          width: 360,
          height: 680,
          opacity: change ? 0 : 1,
          transition: "all 1.5s ease-in-out 0s",
          zIndex: change ? -1 : 1
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(theme_ui__WEBPACK_IMPORTED_MODULE_2__["Image"], {
          src: "/images/covid.png",
          alt: "image"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 9
      }, undefined), change && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Boarding__WEBPACK_IMPORTED_MODULE_4__["default"], {
        object: Objects[next],
        onClick: handleClick,
        onSkip: handleSkip
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 11
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 7
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 55,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (index);

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "theme-ui":
/*!***************************!*\
  !*** external "theme-ui" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("theme-ui");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9Cb2FyZGluZy50c3giLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXgudHN4Iiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQvcm91dGVyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ0aGVtZS11aVwiIl0sIm5hbWVzIjpbIkJvYXJkaW5nIiwib2JqZWN0Iiwib25DbGljayIsIm9uU2tpcCIsImZsZXhEaXJlY3Rpb24iLCJqdXN0aWZ5Q29udGVudCIsImhlaWdodCIsImFsaWduSXRlbXMiLCJpbWFnZSIsImZvbnRTaXplIiwiZm9udFdlaWdodCIsInRpdGxlIiwibGluZUhlaWdodCIsImNvbnRlbnQiLCJjdXJzb3IiLCJ0cmFuc2l0aW9uIiwib3BhY2l0eSIsInRyYW5zZm9ybSIsImlkIiwiYm9yZGVyUmFkaXVzIiwid2lkdGgiLCJPYmplY3RzIiwiaW5kZXgiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJuZXh0Iiwic2V0TmV4dCIsInVzZVN0YXRlIiwiY2hhbmdlIiwic2V0Q2hhbmdlIiwidXNlRWZmZWN0IiwidGltZXIiLCJzZXRUaW1lb3V0IiwiY2xlYXJUaW1lb3V0IiwiaGFuZGxlQ2xpY2siLCJwdXNoIiwiaGFuZGxlU2tpcCIsInBvc2l0aW9uIiwiekluZGV4Il0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZBO0FBQ0E7O0FBZUEsTUFBTUEsUUFBMkIsR0FBRyxDQUFDO0FBQUVDLFFBQUY7QUFBVUMsU0FBVjtBQUFtQkM7QUFBbkIsQ0FBRCxLQUFpQztBQUNuRSxzQkFDRSxxRUFBQyw2Q0FBRDtBQUNFLE1BQUUsRUFBQyxTQURMO0FBRUUsTUFBRSxFQUFFO0FBQ0ZDLG1CQUFhLEVBQUUsUUFEYjtBQUVGQyxvQkFBYyxFQUFFO0FBRmQsS0FGTjtBQUFBLDRCQU9FLHFFQUFDLDZDQUFEO0FBQ0UsUUFBRSxFQUFFLEVBRE47QUFFRSxRQUFFLEVBQUU7QUFDRkMsY0FBTSxFQUFFLEtBRE47QUFFRkMsa0JBQVUsRUFBRSxRQUZWO0FBR0ZGLHNCQUFjLEVBQUU7QUFIZCxPQUZOO0FBQUEsNkJBUUUscUVBQUMsOENBQUQ7QUFBTyxXQUFHLEVBQUVKLE1BQU0sQ0FBQ08sS0FBbkI7QUFBMEIsZUFBTyxFQUFDO0FBQWxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBGLGVBaUJFLHFFQUFDLDZDQUFEO0FBQU0sUUFBRSxFQUFFO0FBQUVKLHFCQUFhLEVBQUU7QUFBakIsT0FBVjtBQUFBLDhCQUNFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLE9BRFI7QUFFRSxVQUFFLEVBQUUsRUFGTjtBQUdFLFVBQUUsRUFBRSxFQUhOO0FBSUUsVUFBRSxFQUFFO0FBQ0ZLLGtCQUFRLEVBQUUsRUFEUjtBQUVGQyxvQkFBVSxFQUFFO0FBRlYsU0FKTjtBQUFBLGtCQVNHVCxNQUFNLENBQUNVO0FBVFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQVlFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLE9BRFI7QUFFRSxVQUFFLEVBQUUsRUFGTjtBQUdFLFVBQUUsRUFBRTtBQUNGRixrQkFBUSxFQUFFLENBRFI7QUFFRkMsb0JBQVUsRUFBRSxNQUZWO0FBR0ZFLG9CQUFVLEVBQUU7QUFIVixTQUhOO0FBQUEsa0JBU0dYLE1BQU0sQ0FBQ1k7QUFUVjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFqQkYsZUF5Q0UscUVBQUMsNkNBQUQ7QUFDRSxRQUFFLEVBQUUsRUFETjtBQUVFLFFBQUUsRUFBRSxFQUZOO0FBR0UsUUFBRSxFQUFFO0FBQ0ZSLHNCQUFjLEVBQUUsZUFEZDtBQUVGRSxrQkFBVSxFQUFFO0FBRlYsT0FITjtBQUFBLDhCQVFFLHFFQUFDLDZDQUFEO0FBQ0UsYUFBSyxFQUFDLE9BRFI7QUFFRSxlQUFPLEVBQUVKLE1BRlg7QUFHRSxVQUFFLEVBQUU7QUFDRk0sa0JBQVEsRUFBRSxFQURSO0FBRUZDLG9CQUFVLEVBQUUsU0FGVjtBQUdGSSxnQkFBTSxFQUFFLFNBSE47QUFJRkMsb0JBQVUsRUFBRSx5QkFKVjtBQUtGQyxpQkFBTyxFQUFFLEdBTFA7QUFNRixvQkFBVTtBQUNSQSxtQkFBTyxFQUFFO0FBREQsV0FOUjtBQVNGLHFCQUFXO0FBQ1RDLHFCQUFTLEVBQUU7QUFERjtBQVRULFNBSE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBUkYsZUEyQkUscUVBQUMsNkNBQUQ7QUFBTSxVQUFFLEVBQUUsRUFBVjtBQUFBLGdDQUNFLHFFQUFDLDRDQUFEO0FBQ0UsWUFBRSxFQUFFaEIsTUFBTSxDQUFDaUIsRUFBUCxLQUFjLENBQWQsR0FBa0IsT0FBbEIsR0FBNEIsU0FEbEM7QUFFRSxXQUFDLEVBQUMsS0FGSjtBQUdFLFlBQUUsRUFBRTtBQUNGQyx3QkFBWSxFQUFFLEdBRFo7QUFFRkMsaUJBQUssRUFBRSxDQUZMO0FBR0ZkLGtCQUFNLEVBQUU7QUFITjtBQUhOO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREYsZUFVRSxxRUFBQyw0Q0FBRDtBQUNFLFlBQUUsRUFBRUwsTUFBTSxDQUFDaUIsRUFBUCxLQUFjLENBQWQsR0FBa0IsT0FBbEIsR0FBNEIsU0FEbEM7QUFFRSxXQUFDLEVBQUMsS0FGSjtBQUdFLFlBQUUsRUFBRTtBQUNGQyx3QkFBWSxFQUFFLEdBRFo7QUFFRkMsaUJBQUssRUFBRSxDQUZMO0FBR0ZkLGtCQUFNLEVBQUU7QUFITjtBQUhOO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBVkYsZUFtQkUscUVBQUMsNENBQUQ7QUFDRSxXQUFDLEVBQUMsS0FESjtBQUVFLFlBQUUsRUFBRUwsTUFBTSxDQUFDaUIsRUFBUCxLQUFjLENBQWQsR0FBa0IsT0FBbEIsR0FBNEIsU0FGbEM7QUFHRSxZQUFFLEVBQUU7QUFDRkMsd0JBQVksRUFBRSxHQURaO0FBRUZDLGlCQUFLLEVBQUUsQ0FGTDtBQUdGZCxrQkFBTSxFQUFFO0FBSE47QUFITjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQW5CRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBM0JGLGVBd0RFLHFFQUFDLCtDQUFEO0FBQVEsZUFBTyxFQUFFSixPQUFqQjtBQUEwQixlQUFPLEVBQUMsU0FBbEM7QUFBQSwrQkFDRSxxRUFBQyw2Q0FBRDtBQUNFLGVBQUssRUFBQyxpQkFEUjtBQUVFLFlBQUUsRUFBRTtBQUNGUSxzQkFBVSxFQUFFLFNBRFY7QUFFRkQsb0JBQVEsRUFBRTtBQUZSLFdBRk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXhERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBekNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBZ0hELENBakhEOztBQW1IZVQsdUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbklBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTXFCLE9BQU8sR0FBRyxDQUNkO0FBQ0VILElBQUUsRUFBRSxDQUROO0FBRUVWLE9BQUssRUFBRSx1QkFGVDtBQUdFRyxPQUFLLEVBQUUsT0FIVDtBQUlFRSxTQUFPLEVBQ0w7QUFMSixDQURjLEVBUWQ7QUFDRUssSUFBRSxFQUFFLENBRE47QUFFRVYsT0FBSyxFQUFFLHVCQUZUO0FBR0VHLE9BQUssRUFBRSxPQUhUO0FBSUVFLFNBQU8sRUFDTDtBQUxKLENBUmMsRUFlZDtBQUNFSyxJQUFFLEVBQUUsQ0FETjtBQUVFVixPQUFLLEVBQUUsdUJBRlQ7QUFHRUcsT0FBSyxFQUFFLHNCQUhUO0FBSUVFLFNBQU8sRUFDTDtBQUxKLENBZmMsQ0FBaEI7O0FBd0JBLE1BQU1TLEtBQVMsR0FBRyxNQUFNO0FBQ3RCLFFBQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsSUFBRDtBQUFBLE9BQU9DO0FBQVAsTUFBa0JDLHNEQUFRLENBQUMsQ0FBRCxDQUFoQztBQUNBLFFBQU07QUFBQSxPQUFDQyxNQUFEO0FBQUEsT0FBU0M7QUFBVCxNQUFzQkYsc0RBQVEsQ0FBQyxLQUFELENBQXBDO0FBRUFHLHlEQUFTLENBQUMsTUFBTTtBQUNkLFVBQU1DLEtBQUssR0FBR0MsVUFBVSxDQUFDLE1BQU07QUFDN0JILGVBQVMsQ0FBQyxJQUFELENBQVQ7QUFDRCxLQUZ1QixFQUVyQixJQUZxQixDQUF4QjtBQUdBLFdBQU8sTUFBTUksWUFBWSxDQUFDRixLQUFELENBQXpCO0FBQ0QsR0FMUSxFQUtOLEVBTE0sQ0FBVDs7QUFPQSxRQUFNRyxXQUFXLEdBQUcsTUFBTTtBQUN4QixRQUFJVCxJQUFJLEtBQUssQ0FBYixFQUFnQjtBQUNkRixZQUFNLENBQUNZLElBQVAsQ0FBWSxZQUFaO0FBQ0QsS0FGRCxNQUVPO0FBQ0xULGFBQU8sQ0FBQ0QsSUFBSSxHQUFHLENBQVIsQ0FBUDtBQUNEO0FBQ0YsR0FORDs7QUFRQSxRQUFNVyxVQUFVLEdBQUcsTUFBTTtBQUN2QmIsVUFBTSxDQUFDWSxJQUFQLENBQVksWUFBWjtBQUNELEdBRkQ7O0FBSUEsc0JBQ0UscUVBQUMsNkNBQUQ7QUFDRSxNQUFFLEVBQUU7QUFDRmYsV0FBSyxFQUFFLE1BREw7QUFFRmYsb0JBQWMsRUFBRSxRQUZkO0FBR0ZFLGdCQUFVLEVBQUU7QUFIVixLQUROO0FBQUEsMkJBT0UscUVBQUMsNkNBQUQ7QUFDRSxRQUFFLEVBQUU7QUFDRmEsYUFBSyxFQUFFLEdBREw7QUFFRmQsY0FBTSxFQUFFO0FBRk4sT0FETjtBQUFBLDhCQU1FLHFFQUFDLDZDQUFEO0FBQ0UsVUFBRSxFQUFFO0FBQ0YrQixrQkFBUSxFQUFFLFVBRFI7QUFFRmpCLGVBQUssRUFBRSxHQUZMO0FBR0ZkLGdCQUFNLEVBQUUsR0FITjtBQUlGVSxpQkFBTyxFQUFFWSxNQUFNLEdBQUcsQ0FBSCxHQUFPLENBSnBCO0FBS0ZiLG9CQUFVLEVBQUUseUJBTFY7QUFNRnVCLGdCQUFNLEVBQUVWLE1BQU0sR0FBRyxDQUFDLENBQUosR0FBUTtBQU5wQixTQUROO0FBQUEsK0JBVUUscUVBQUMsOENBQUQ7QUFBTyxhQUFHLEVBQUMsbUJBQVg7QUFBK0IsYUFBRyxFQUFDO0FBQW5DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWRjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQU5GLEVBbUJHQSxNQUFNLGlCQUNMLHFFQUFDLDREQUFEO0FBQ0UsY0FBTSxFQUFFUCxPQUFPLENBQUNJLElBQUQsQ0FEakI7QUFFRSxlQUFPLEVBQUVTLFdBRlg7QUFHRSxjQUFNLEVBQUVFO0FBSFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFwQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBcUNELENBN0REOztBQStEZWQsb0VBQWYsRTs7Ozs7Ozs7Ozs7QUM1RkEsd0M7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsa0Q7Ozs7Ozs7Ozs7O0FDQUEscUMiLCJmaWxlIjoicGFnZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3BhZ2VzL2luZGV4LnRzeFwiKTtcbiIsImltcG9ydCBSZWFjdCwgeyBGQyB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGbGV4LCBUZXh0LCBJbWFnZSwgQnV0dG9uLCBCb3ggfSBmcm9tIFwidGhlbWUtdWlcIjtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgT2JqZWN0UHJvcHMge1xyXG4gIGlkOiBudW1iZXI7XHJcbiAgaW1hZ2U6IHN0cmluZztcclxuICB0aXRsZTogc3RyaW5nO1xyXG4gIGNvbnRlbnQ6IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBCb2FyZGluZ1Byb3BzIHtcclxuICBvYmplY3Q6IE9iamVjdFByb3BzO1xyXG4gIG9uQ2xpY2s/OiAoKSA9PiB2b2lkO1xyXG4gIG9uU2tpcD86ICgpID0+IHZvaWQ7XHJcbn1cclxuXHJcbmNvbnN0IEJvYXJkaW5nOiBGQzxCb2FyZGluZ1Byb3BzPiA9ICh7IG9iamVjdCwgb25DbGljaywgb25Ta2lwIH0pID0+IHtcclxuICByZXR1cm4gKFxyXG4gICAgPEZsZXhcclxuICAgICAgYmc9XCIjRUQzRDNEXCJcclxuICAgICAgc3g9e3tcclxuICAgICAgICBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLFxyXG4gICAgICAgIGp1c3RpZnlDb250ZW50OiBcInNwYWNlLWJldHdlZW5cIixcclxuICAgICAgfX1cclxuICAgID5cclxuICAgICAgPEZsZXhcclxuICAgICAgICBtdD17NTB9XHJcbiAgICAgICAgc3g9e3tcclxuICAgICAgICAgIGhlaWdodDogXCI0MCVcIixcclxuICAgICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICB9fVxyXG4gICAgICA+XHJcbiAgICAgICAgPEltYWdlIHNyYz17b2JqZWN0LmltYWdlfSB2YXJpYW50PVwiYm9hcmRpbmdcIiAvPlxyXG4gICAgICA8L0ZsZXg+XHJcbiAgICAgIDxGbGV4IHN4PXt7IGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIgfX0+XHJcbiAgICAgICAgPFRleHRcclxuICAgICAgICAgIGNvbG9yPVwid2hpdGVcIlxyXG4gICAgICAgICAgbXg9ezI2fVxyXG4gICAgICAgICAgbWI9ezEwfVxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgZm9udFNpemU6IDIyLFxyXG4gICAgICAgICAgICBmb250V2VpZ2h0OiBcImhlYWRpbmdcIixcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAge29iamVjdC50aXRsZX1cclxuICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgPFRleHRcclxuICAgICAgICAgIGNvbG9yPVwid2hpdGVcIlxyXG4gICAgICAgICAgbXg9ezI2fVxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgZm9udFNpemU6IDEsXHJcbiAgICAgICAgICAgIGZvbnRXZWlnaHQ6IFwiYm9keVwiLFxyXG4gICAgICAgICAgICBsaW5lSGVpZ2h0OiBcIjI0cHhcIixcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAge29iamVjdC5jb250ZW50fVxyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgPC9GbGV4PlxyXG4gICAgICA8RmxleFxyXG4gICAgICAgIG14PXsyMH1cclxuICAgICAgICBtYj17MzB9XHJcbiAgICAgICAgc3g9e3tcclxuICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcInNwYWNlLWJldHdlZW5cIixcclxuICAgICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgfX1cclxuICAgICAgPlxyXG4gICAgICAgIDxUZXh0XHJcbiAgICAgICAgICBjb2xvcj1cIndoaXRlXCJcclxuICAgICAgICAgIG9uQ2xpY2s9e29uU2tpcH1cclxuICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAxNSxcclxuICAgICAgICAgICAgZm9udFdlaWdodDogXCJoZWFkaW5nXCIsXHJcbiAgICAgICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IFwiYWxsIDAuM3MgZWFzZS1pbi1vdXQgMHNcIixcclxuICAgICAgICAgICAgb3BhY2l0eTogMC44LFxyXG4gICAgICAgICAgICBcIjpob3ZlclwiOiB7XHJcbiAgICAgICAgICAgICAgb3BhY2l0eTogMSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCI6YWN0aXZlXCI6IHtcclxuICAgICAgICAgICAgICB0cmFuc2Zvcm06IFwic2NhbGUoMC45NSlcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgU2tpcFxyXG4gICAgICAgIDwvVGV4dD5cclxuICAgICAgICA8RmxleCBtbD17MzV9PlxyXG4gICAgICAgICAgPEJveFxyXG4gICAgICAgICAgICBiZz17b2JqZWN0LmlkID09PSAxID8gXCJ3aGl0ZVwiIDogXCIjODcwMDAwXCJ9XHJcbiAgICAgICAgICAgIG09XCIycHhcIlxyXG4gICAgICAgICAgICBzeD17e1xyXG4gICAgICAgICAgICAgIGJvcmRlclJhZGl1czogOTk5LFxyXG4gICAgICAgICAgICAgIHdpZHRoOiA3LFxyXG4gICAgICAgICAgICAgIGhlaWdodDogNyxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgID48L0JveD5cclxuICAgICAgICAgIDxCb3hcclxuICAgICAgICAgICAgYmc9e29iamVjdC5pZCA9PT0gMiA/IFwid2hpdGVcIiA6IFwiIzg3MDAwMFwifVxyXG4gICAgICAgICAgICBtPVwiMnB4XCJcclxuICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICBib3JkZXJSYWRpdXM6IDk5OSxcclxuICAgICAgICAgICAgICB3aWR0aDogNyxcclxuICAgICAgICAgICAgICBoZWlnaHQ6IDcsXHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICA+PC9Cb3g+XHJcbiAgICAgICAgICA8Qm94XHJcbiAgICAgICAgICAgIG09XCIycHhcIlxyXG4gICAgICAgICAgICBiZz17b2JqZWN0LmlkID09PSAzID8gXCJ3aGl0ZVwiIDogXCIjODcwMDAwXCJ9XHJcbiAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgYm9yZGVyUmFkaXVzOiA5OTksXHJcbiAgICAgICAgICAgICAgd2lkdGg6IDcsXHJcbiAgICAgICAgICAgICAgaGVpZ2h0OiA3LFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPjwvQm94PlxyXG4gICAgICAgIDwvRmxleD5cclxuICAgICAgICA8QnV0dG9uIG9uQ2xpY2s9e29uQ2xpY2t9IHZhcmlhbnQ9XCJwcmltYXJ5XCI+XHJcbiAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICBjb2xvcj1cImNoZWxzZWFDdWN1bWJlclwiXHJcbiAgICAgICAgICAgIHN4PXt7XHJcbiAgICAgICAgICAgICAgZm9udFdlaWdodDogXCJoZWFkaW5nXCIsXHJcbiAgICAgICAgICAgICAgZm9udFNpemU6IDE1LFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBOZXh0XHJcbiAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgIDwvRmxleD5cclxuICAgIDwvRmxleD5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgQm9hcmRpbmc7XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIEZDLCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgSW1hZ2UsIEZsZXggfSBmcm9tIFwidGhlbWUtdWlcIjtcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xuaW1wb3J0IEJvYXJkaW5nIGZyb20gXCIuLi9jb21wb25lbnRzL0JvYXJkaW5nXCI7XG5cbmNvbnN0IE9iamVjdHMgPSBbXG4gIHtcbiAgICBpZDogMSxcbiAgICBpbWFnZTogXCIvaW1hZ2VzL2JvYXJkaW5nMS5QTkdcIixcbiAgICB0aXRsZTogXCJGZXZlclwiLFxuICAgIGNvbnRlbnQ6XG4gICAgICBcIkhlIHNldmVyaXR5IG9mIENPVklELTE5IHN5bXB0b21zIGNhbiByYW5nZSBmcm9tIHZlcnkgbWlsZCB0byBzZXZlcmUuIFNvbWUgcGVvcGxlIGhhdmUgbm8gc3ltcHRvbXMuIFBlb3BsZSB3aG8gYXJlIG9sZGVyIG9yIGhhdmUgZXhpc3RpbmcgY2hyb25pYyAgbWVkaWNhbCBjb25kaXRpb25zLlwiLFxuICB9LFxuICB7XG4gICAgaWQ6IDIsXG4gICAgaW1hZ2U6IFwiL2ltYWdlcy9ib2FyZGluZzIuUE5HXCIsXG4gICAgdGl0bGU6IFwiQ291Z2hcIixcbiAgICBjb250ZW50OlxuICAgICAgXCJzdWNoIGFzIGhlYXJ0IG9yIGx1bmcgZGlzZWFzZSBvciBkaWFiZXRlcywgbWF5IGJlIGF0IGhpZ2hlciByaXNrIG9mIHNlcmlvdXMgaWxsbmVzcy4gVGhpcyBpcyBzaW1pbGFyIHRvIHdoYXQgaXMgc2VlbiB3aXRoIG90aGVyIHJlc3BpcmF0b3J5IGlsbG5lc3Nlcywgc3VjaCBhcyBpbmZsdWVuemEuXCIsXG4gIH0sXG4gIHtcbiAgICBpZDogMyxcbiAgICBpbWFnZTogXCIvaW1hZ2VzL2JvYXJkaW5nMy5QTkdcIixcbiAgICB0aXRsZTogXCJCcmVhdGhpbmcgRGlmZmljdWx0eVwiLFxuICAgIGNvbnRlbnQ6XG4gICAgICBcIkNvbnRhY3QgeW91ciBkb2N0b3Igb3IgY2xpbmljIHJpZ2h0IGF3YXkgaWYgeW91IGhhdmUgQ09WSUQtMTkgc3ltcHRvbXMsIHlvdSd2ZSBiZWVuIGV4cG9zZWQgdG8gc29tZW9uZSB3aXRoIENPVklELTE5LCBvciB5b3UgbGl2ZSBpbiBvciBoYXZlIHRyYXZlbGVkIGZyb20gYW4gYXJlYSB3aXRoIG9uZ29pbmcgY29tbXVuaXR5IHNwcmVhZCBvZiBDT1ZJRC0xOVwiLFxuICB9LFxuXTtcblxuY29uc3QgaW5kZXg6IEZDID0gKCkgPT4ge1xuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcbiAgY29uc3QgW25leHQsIHNldE5leHRdID0gdXNlU3RhdGUoMCk7XG4gIGNvbnN0IFtjaGFuZ2UsIHNldENoYW5nZV0gPSB1c2VTdGF0ZShmYWxzZSk7XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBjb25zdCB0aW1lciA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgc2V0Q2hhbmdlKHRydWUpO1xuICAgIH0sIDIwMDApO1xuICAgIHJldHVybiAoKSA9PiBjbGVhclRpbWVvdXQodGltZXIpO1xuICB9LCBbXSk7XG5cbiAgY29uc3QgaGFuZGxlQ2xpY2sgPSAoKSA9PiB7XG4gICAgaWYgKG5leHQgPT09IDIpIHtcbiAgICAgIHJvdXRlci5wdXNoKFwiL2Rhc2hib2FyZFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc2V0TmV4dChuZXh0ICsgMSk7XG4gICAgfVxuICB9O1xuXG4gIGNvbnN0IGhhbmRsZVNraXAgPSAoKSA9PiB7XG4gICAgcm91dGVyLnB1c2goXCIvZGFzaGJvYXJkXCIpO1xuICB9O1xuXG4gIHJldHVybiAoXG4gICAgPEZsZXhcbiAgICAgIHN4PXt7XG4gICAgICAgIHdpZHRoOiBcIjEwMCVcIixcbiAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwiY2VudGVyXCIsXG4gICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXG4gICAgICB9fVxuICAgID5cbiAgICAgIDxGbGV4XG4gICAgICAgIHN4PXt7XG4gICAgICAgICAgd2lkdGg6IDM2MCxcbiAgICAgICAgICBoZWlnaHQ6IDY4MCxcbiAgICAgICAgfX1cbiAgICAgID5cbiAgICAgICAgPEZsZXhcbiAgICAgICAgICBzeD17e1xuICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcbiAgICAgICAgICAgIHdpZHRoOiAzNjAsXG4gICAgICAgICAgICBoZWlnaHQ6IDY4MCxcbiAgICAgICAgICAgIG9wYWNpdHk6IGNoYW5nZSA/IDAgOiAxLFxuICAgICAgICAgICAgdHJhbnNpdGlvbjogXCJhbGwgMS41cyBlYXNlLWluLW91dCAwc1wiLFxuICAgICAgICAgICAgekluZGV4OiBjaGFuZ2UgPyAtMSA6IDEsXG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxJbWFnZSBzcmM9XCIvaW1hZ2VzL2NvdmlkLnBuZ1wiIGFsdD1cImltYWdlXCIgLz5cbiAgICAgICAgPC9GbGV4PlxuXG4gICAgICAgIHtjaGFuZ2UgJiYgKFxuICAgICAgICAgIDxCb2FyZGluZ1xuICAgICAgICAgICAgb2JqZWN0PXtPYmplY3RzW25leHRdfVxuICAgICAgICAgICAgb25DbGljaz17aGFuZGxlQ2xpY2t9XG4gICAgICAgICAgICBvblNraXA9e2hhbmRsZVNraXB9XG4gICAgICAgICAgLz5cbiAgICAgICAgKX1cbiAgICAgIDwvRmxleD5cbiAgICA8L0ZsZXg+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBpbmRleDtcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvcm91dGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ0aGVtZS11aVwiKTsiXSwic291cmNlUm9vdCI6IiJ9